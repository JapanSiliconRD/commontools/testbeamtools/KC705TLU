#include "TLU_sitcp.h"
#include "DACctrl.h"

void ExecDacCtrl(TLU_sitcp *sitcp,int chan,int int_cmd,int dacadd,int dacval){
  if ( int_cmd == 1 ) {
    std::cout << "    DAC Initialization : " ;
    DACTLVInit( sitcp, chan ) ;
     std::cout << "OK" << std::endl ;
    
  } else if ( int_cmd == 2 ) {
    std::cout << "    0: DAC Load OFF, 1: ON : " << dacadd << " ";
    DACTLVLoad( sitcp,chan, dacadd ) ;
    std::cout << "OK" << std::endl ;
    
  } else if ( int_cmd == 3 ) {
    std::cout << "    0: DAC Preset OFF, 1: ON : " << dacadd << " ";
    DACTLVPreset( sitcp,chan, dacadd ) ;
    std::cout << "OK" << std::endl ;
  } else if ( int_cmd == 4 ) {
    std::cout << "    Write addres and data: "  << dacadd << " " << dacval << " ";
    DACTLVWriteData( sitcp,chan, dacadd, dacval ) ; 
    usleep(1000) ;
    DACTLVWrite( sitcp,chan ) ;
    std::cout << "OK" << std::endl ;    
  }
}
void ExecDacCtrlInt(TLU_sitcp *sitcp){
  int chan=0;
    while (1) {
        std::cout << "Input command:" << std::endl ;
        std::cout << "    1 : Reset/Init." << std::endl ;
        std::cout << "    2 : Load enable" << std::endl ;
        std::cout << "    3 : Data preset" << std::endl ;
        std::cout << "    4 : Data write" << std::endl ;  
        std::cout << "    5 : MPPC channel" << std::endl ;  
		std::cout << "    0 : Quit" << std::endl ;
        std::cout << "Command: " ;          

        std::string str_cmd ;
        std::cin >> str_cmd ;
        int int_cmd = atoi( str_cmd.c_str() ) ;
        
		std::string str_addr, str_data ;  
		int int_addr, int_data ;  
        if ( int_cmd == 1 ) {
 		   std::cout << "    DAC Initialization : " ;
    
		   DACTLVInit( sitcp, chan) ;
       
           std::cout << "OK" << std::endl ;
 
		} else if ( int_cmd == 2 ) {
 		   std::cout << "    0: DAC Load OFF, 1: ON : " ;
           std::cin >> str_data ;

           int_data = atoi( str_data.c_str() ) ;
           DACTLVLoad( sitcp, chan, int_data ) ;

		} else if ( int_cmd == 3 ) {
 		   std::cout << "    0: DAC Preset OFF, 1: ON : " ;
           std::cin >> str_data ;

           int_data = atoi( str_data.c_str() ) ;
           DACTLVPreset( sitcp, chan, int_data ) ;

		} else if ( int_cmd == 4 ) {
		   std::cout << "    Write addres and data: "  ;
           std::cin >> str_addr >> str_data ;

           int_addr = atoi( str_addr.c_str() ) ;
           int_data = atoi( str_data.c_str() ) ;

           DACTLVWriteData( sitcp, chan, int_addr, int_data ) ; 
           usleep(1000) ;
           DACTLVWrite( sitcp,chan ) ;
           

		} else if ( int_cmd == 5 ) {
	  std::cout << "    Choose MPPC channel [0-3] (" << chan << ") : " ;
           std::cin >> str_data ;

           chan = atoi(str_data.c_str());
	  
		} else if ( int_cmd == 0 ) {
           std::cout << "End" << std:: endl ;
           break ;
        }
       
        
    }
}

int getChanOffset(int chan){
  if(chan==0)return 0;
  else if(chan==1) return 0xa;
  else if(chan==2) return 0x10;
  else if(chan==3) return 0x1a;
  else {
    std::cout << "channel : " << chan << " should be 0-3 "<< std::endl;
    return -1;
  }
}
void DACTLVWrite( TLU_sitcp *stcp,int chan ) 
{
  // Data writing
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_WRITE, 1 ) ;
  usleep(1000) ;
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_WRITE, 0 ) ;
  usleep(1000) ;

  return ;  
}


void DACTLVWriteData( TLU_sitcp *stcp,int chan, int addr, int data ) 
{
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_ADD, addr ) ;
  usleep(1000) ;
  
  int data_l = ( data & 0x00FF ) ;
  int data_u = ( data & 0xFF00 ) >> 8 ;
  std::cout << "<<<< " <<  getChanOffset(chan)+ADDR_DACTLV_DATA_L << " " << data_l << std::endl;
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_DATA_L, data_l ) ;
  usleep(1000) ;

  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_DATA_U, data_u ) ;

  return ;  
}



void DACTLVLoad( TLU_sitcp *stcp,int chan , int int_data) 
{
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_LOAD, int_data ) ;
  return ;  
}


void DACTLVPreset( TLU_sitcp *stcp,int chan, int int_data ) 
{
  stcp->WriteRBCP( getChanOffset(chan)+ADDR_DACTLV_PRESET, int_data ) ;
  return ;  
}

void DACTLVInit( TLU_sitcp *stcp, int chan ) 
{
  // DAC Load ON
  DACTLVLoad( stcp, chan, 1 ) ;
  usleep(1000) ;
  
  // DAC Preset ON -> OFF
  DACTLVPreset( stcp, chan, 1  ) ;
  usleep(1000) ;
  DACTLVPreset( stcp, chan, 0 ) ;
  usleep(1000) ;

  // Ctrl 0
  int addr = 8 ;  
  int data = 6 ; // DOUT: DE, Ref: 2V
  DACTLVWriteData( stcp, chan, addr, data ) ;  
  usleep(1000) ;
 
  DACTLVWrite( stcp, chan ) ;
  usleep(1000) ;

  return ;  
}

