#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <ctime>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include "TLU_sitcp.h"
#include <iomanip>
#include "DACctrl.h"
//#define IpAddr "192.168.0.16" //for SEABAS1
#define IpAddr "192.168.10.16" //for SEABAS2
#define tcpport 24
#define udpport 4660

#define binary(bit) strtol(bit,NULL,2)

void setdacconfig(TLU_sitcp* sitcp,int add,int value);


using namespace std;

int main(int argc, char *argv[])
{
  bool debug=false;
  std::string filename="tmp";
  std::string runtype="startrun";
  int nevent=-1;
  int dacmode=0;
  int chan=0;
  int dacadd=0;
  int dacval=0;
  //==================================================================
  //	Acquiring run mode.
  //==================================================================

  if(argc>1){
    runtype=argv[1];
    std::cout << "set runtype : " << runtype << std::endl;
  }else{
    std::cout << "set runtype : " << runtype << std::endl;
  }
  if(runtype!="dacconfig"){
    if(argc>2){
      filename=argv[2];
      std::cout << "File name was set : " << filename << ".dat and " << filename << ".root"  << std::endl;
      if(argc>3){
	nevent=atoi(argv[3]);	
      }
    }else{
      std::cout << "Default file name was set : tmp.dat and tmp.root"  << std::endl;
    }
  }else{
    if(argc>2){
      dacmode=atoi(argv[2]);
      if(dacmode==0){
	std::cout << "" << dacmode << std::endl;
      }else if(dacmode==1){
	if(argc>5){
	  chan=atoi(argv[3]);
	  std::cout << chan << std::endl;
	  dacadd=atoi(argv[4]);
	  std::cout << dacadd << std::endl;
	  dacval=atoi(argv[5]);
	  std::cout << dacval << std::endl;
	}else{
	  std::cout << "usage : ./rundaq dacconfig 1 [mppcchan] [dacadd] [dacval]" << std::endl;
	  return -1;
	}
      }
    }else{
      std::cout << "intaractive mode  : ./rundaq dacconfig 0" << std::endl;
      std::cout << "automatic mode    : ./rundaq dacconfig 1 [mppcchan] [dacadd] [dacval]" << std::endl;
      return -1;
    }
  }
  

  if( (access( ("./data/"+filename+".dat").c_str(), 0 )) != -1 
      ||  (access( ("./data/"+filename+".bit").c_str(), 0 )) != -1 ){
    std::cout << "file "  << filename << ".dat/.bit exist" << std::endl;
    return -1;
  }else{
    std::cout << ""  << filename << ".dat/.bit will be created" << std::endl;
  }

  //  return 0;
  //==================================================================
  //	Initializing modules.
  //==================================================================
  TLU_sitcp* sitcp = new TLU_sitcp();

  //==================================================================
  //	Setting up connection with SEABAS.
  //==================================================================
  sitcp -> SetIPPort(IpAddr, tcpport, udpport);
  //  std::cout << "creating TCP Socket ..." << std::endl;
  if(runtype!="stoprun"&&runtype!="dacconfig") sitcp -> CreateTCPSock();
  //  std::cout << "creating UDP Socket ..." << std::endl;
  sitcp -> CreateUDPSock();
  //  std::cout << "done." << std::endl;

  if(runtype=="stoprun"){
    int data=0xf5;
    //    int data=0xff;
    //    while(1){
    std::cout << "sending stop run data(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
    sitcp->WriteRBCP(0,data);
    //    sitcp->SendWriteRbcpPackets("Sending test command");
    //    }
    return 0;
  }else if(runtype=="startrun"){
    int data=0xfa;
    //    int data=0xff;
    std::cout << "sending start run data(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
    sitcp->WriteRBCP(0,data);
  }else if(runtype=="trigenable"){
    int data=0xfa;
    //    int data=0xff;
    std::cout << "sending trigger enable data(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
    sitcp->WriteRBCP(0,data);
    return 0;
  }else   if(runtype=="dacconfig"){
    if(dacmode==0){
      ExecDacCtrlInt(sitcp);
    }if(dacmode==1){
      if(dacadd==99&&dacval==1){
	ExecDacCtrl(sitcp,chan,1);
      }else if (dacadd==99&&dacval==2){
	ExecDacCtrl(sitcp,chan,2,1);
      }else{
	//    ExecDacCtrl(sitcp,3,??);
	ExecDacCtrl(sitcp,chan,4,dacadd,dacval);
      }
    }
    //    std::cout << dacadd << std::endl;
//    setdacconfig(sitcp,dacadd,dacval);
//    int data=0xf1;
//    std::cout << "sending dac config(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
//    //    sleep(1);
//    sitcp->WriteRBCP(0,data);
//    data=0xf2;
//    std::cout << "sending dac lv enable(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
//    //    sleep(1);
//    sitcp->WriteRBCP(0,data);
    return 0;
  }else{
    std::cout << "runtype : " << runtype << " does not match." << std::endl;
    return -1;
  }


  int dataout[6]={0,0,0,0,0,0};
  fd_set fdset;
  FD_ZERO(&fdset);
  FD_SET(sitcp -> GetTCPSock(),&fdset);
  std::ofstream outfile;
  int runnumber=1000;
  time_t starttime=std::time(0);
  
  outfile.open(("./data/"+filename+".bin").c_str(),std::ios::out | std::ios::binary);
  outfile.write((char*)&runnumber,sizeof(int));
  outfile.write((char*)&starttime,sizeof(time_t));
  int jj=0;
  
  //  int totalnevent=2000;
  //  int totalnevent=-1;
  int totalnevent=nevent;
  //  while(jj<100000000){
  //  int showeventper=1;
  int showeventper=1000;
  if(debug)showeventper=1;
  struct timeval start;
  struct timeval laststart;
  struct timeval stop;
  gettimeofday(&start,NULL);
  gettimeofday(&laststart,NULL);
  while(1){
    outfile.write((char*)&jj,sizeof(int));
    if(jj%showeventper==0){
      gettimeofday(&stop,NULL);
      double timefromstart=(double)(stop.tv_sec+stop.tv_usec/1.0e6-start.tv_sec+start.tv_usec/1.0e6);
      double timefromlast=(double)(stop.tv_sec+stop.tv_usec/1.0e6-laststart.tv_sec+laststart.tv_usec/1.0e6);
      std::cout <<std::dec<< jj << "th Event Processed " << std::setprecision(3) << timefromstart/60. << "min " << (double)jj/timefromstart << "Hz (" << std::setprecision(4) << (double)showeventper/timefromlast << "Hz)"<< std::endl;
      gettimeofday(&laststart,NULL); 
    }
    //    std::cout << std::dec << jj <<  "   : " ;
    //    std::cout << "."  << std::endl;
    struct timeval timeout;
    timeout.tv_sec=0;
    timeout.tv_usec=1e9;
    int returnval=select(sitcp -> GetTCPSock()+1,&fdset,NULL, NULL,&timeout);
    //    std::cout << returnval << std::endl;
    if(returnval==0){
      std::cout << " select() timeout ..." << std::endl;
      //      break;
      continue;
    }else if(returnval<-1){
      std::cout << "select() returned  " << returnval << std::endl;
      return -1;
    }
    
    
    if(returnval==1){
      if(0){
      	char test[256];
      	recv(sitcp -> GetTCPSock(), test, 11, 0);// recieve
      	for(int ii=0;ii<11;ii++){
      	  int testi=test[ii]<0?test[ii]+256:test[ii];
      	  std::cout << std::hex << testi << std::dec << " " ;
      	}
      	std::cout<<std::endl;
      	jj++;continue;
      }
      if(1){
	char dataheader=0;
	int dhcounter=0;
	if(debug||jj%showeventper==0) std::cout << "    ===> ";
	while(1){
	  //	recv(sitcp -> GetTCPSock(), &dataheader, 1,0);// recieve
	  recv(sitcp -> GetTCPSock(), &dataheader, 1, MSG_WAITALL);// recieve
	  
	  int dh=dataheader<0?dataheader+256:dataheader;
	  if(debug||jj%showeventper==0) std::cout << std::hex << dh << "  ";
	  if((dh&0xff)==0xff) dhcounter++;
	  if(dhcounter>=4&&(dh&0xf0)==0x0){
	    dhcounter=0;
	    break;
	  }
	}
	
//	recv(sitcp -> GetTCPSock(), dataout, 6, 0);// recieve
//	int rlen=recv(sitcp -> GetTCPSock(), dataout, 6, MSG_WAITALL);// recieve
//	if(debug)std::cout << std::dec << " : " << rlen  << "  "<< std::hex << dataout << std::endl; 
	for(int ii=0;ii<6;ii++){
	  int dout;
	  int rlen=recv(sitcp -> GetTCPSock(), &dout, 1, MSG_WAITALL);// recieve
	  dataout[ii]=dout<0?dout+256:dout;

	  if(debug||jj%showeventper==0){
	    std::cout << std::dec << " " << std::hex << dataout[ii]<< " "; 
	    if(ii==5)std::cout << std::endl;
	  }
	}
	
	int full=0xff;
	//      std::cout <<std::hex << full << std::dec << std::endl;
	for(int ii=0;ii<4;ii++){
	  outfile.write((char*)&full,sizeof(char));
	}
	outfile.write((char*)&dataheader,sizeof(char));
	
	for(int ii=0;ii<6;ii++){
	  outfile.write((char*)&dataout[ii],sizeof(char));
	}
	
      }


      if(jj!=0 && jj%1000==0){
	//	std::cout << "Writing..."<< std::endl;
	outfile.close();
	outfile.open(("./data/"+filename+".bin").c_str(),std::ios::out | std::ios::binary | std::ios::app);
      }
    }
    jj++;
    if(jj==totalnevent){
      int data=0xf5;
      //      int data=0x55;
      std::cout << "sending stop run data(" << std::hex<< data << std::dec<< ") to UTP" << std::endl;
      sitcp->WriteRBCP(0,data);
      break;
    }
  }
  // Finalize.
  sitcp->CloseTCPSock();
  sitcp->CloseUDPSock();
  outfile.close();
  //  std::cout <<  "outfile close shita" << std::endl;

  delete sitcp;

  return 0;
}

void setdacconfig(TLU_sitcp* sitcp, int addr,int value){
  std::cout << "sending data to UTP... " << addr << std::endl;
  std::cout << "set address " << std::hex<< 0x10+(addr&0xf) << std::dec<< "..."<< std::endl;
  sitcp->WriteRBCP(1,0x10+(addr&0xf));
  std::cout << "set value " << std::dec<< value << "..."<< std::endl;
  //  sleep(3);
  std::cout << "         " << std::hex << 0x20+(value&0xf) << std::endl;
  sitcp->WriteRBCP(2,0x20+value&0xf);
  //  sleep(3);
  std::cout << "         " << std::hex << 0x30+((value&0xf0)>>4) << std::endl;
  sitcp->WriteRBCP(3,0x30+((value&0xf0)>>4));
  //  sleep(3);
  std::cout << "         " << 0x40+((value&0xf00)>>8) << std::dec << std::endl;
  sitcp->WriteRBCP(4,0x40+((value&0xf00)>>8));
}

/*
int  readdata(char *dataout, DataStruct &ds){
  int xx[6]={-1,-1,-1,-1,-1,-1};
  for(int ii=0;ii<6;ii++){
    xx[ii]=(int)dataout[ii];
    xx[ii]=xx[ii]<0?xx[ii]+256:xx[ii];
    std::cout << std::hex << std::setw(2)<< xx[ii] << " ";
    //    std::cout << (xx[ii]<0?xx[ii]+256:xx[ii]) << " ";
  }
  ds.DH=xx[0];
  ds.TID0=xx[1];
  ds.TID1=xx[2];
  ds.TDC0=xx[3];
  ds.TDC1=xx[4];
  ds.TDC2=xx[5];

  if((xx[0]&0xf0)==0xa0) ds.TrigType=1;
  else if((xx[0]&0xf0)==0xb0) ds.TrigType=2;
  else if((xx[0]&0xf0)==0xc0) ds.TrigType=3;
  else if((xx[0]&0xf0)==0xd0) ds.TrigType=4;
  else if((xx[0]&0xf0)==0xe0) ds.TrigType=5;
  else  {
    std::cerr << "unknow type detected : " << std::hex << xx[0] << std::dec << std::endl;
    ds.TrigType=0;
    return -1;
  }
  std::cout << std::endl;
  ds.TrigID=xx[1]*256+xx[2];
  ds.TdcCount=xx[3]*256*256+xx[4]*256+xx[5];
  return 0;
}
int  writetofile(DataStruct ds,TTree *tree,std::string filename){
  std::ofstream textout(("./data/"+filename+".dat").c_str(), ios::app);
  if((ds.DH&0xf0)==0xa0)textout << "COSMIC   ";
  else if((ds.DH&0xf0)==0xb0)textout << "MUDECAYST  ";
  else if((ds.DH&0xf0)==0xc0)textout << "NOSTOPDN  ";
  else if((ds.DH&0xf0)==0xd0)textout << "DECAYUP  ";
  else if((ds.DH&0xf0)==0xe0)textout << "DECAYDN  ";
  else {
    return -1;
    textout << "UNKNOWN   ";
  }
  textout << std::dec << std::setw(6) << ds.TrigID << " ";
  textout << setw(9) << ds.TdcCount << "  : bit( " << std::hex ;
  textout << std::setw(2) << ds.DH << " ";
  textout << std::setw(2) << ds.TID0 << " ";
  textout << std::setw(2) << ds.TID1 << " ";
  textout << std::setw(2) << ds.TDC0 << " ";
  textout << std::setw(2) << ds.TDC1 << " ";
  textout << std::setw(2) << ds.TDC2 << " ";
  textout << " )" << endl;
  textout.close();
  tree->Fill();
  tree->Write("",TObject::kOverwrite);
  return 1;
}
*/
