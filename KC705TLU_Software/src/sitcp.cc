#include "sitcp.h"

SiTCP::SiTCP(){
  std::cout<<"SiTCP control --> Start."<<std::endl;
};

SiTCP::~SiTCP(){};

bool SiTCP::SetIPPort(char* IpAddr, unsigned int tcp, unsigned int udp){
  sitcpIpAddr = IpAddr;
  if(tcp<=0 || 65535<=tcp || udp<=0 || 65535<=udp){
    if(tcp<=0 || 65535<=tcp){
      std::cout<<"error : invalid port : tcp = "<<tcp<<std::endl;
    }
    if(udp<=0 || 65535<=udp){
      std::cout<<"error : invalid port : udp = "<<udp<<std::endl;
    }
    exit(1);
  }
  tcpPort = tcp;
  udpPort = udp;
  puts("");
  std::cout<<"SiTCP:IP Address = "<<sitcpIpAddr<<std::endl;
  std::cout<<"      TCP Port   = "<<tcpPort<<std::endl;
  std::cout<<"      UDP Port   = "<<udpPort<<std::endl;
  puts("");
  return true;
}

bool SiTCP::CreateTCPSock(){
  std::cout<<"Create socket for TCP...";
  tcpsock = socket(AF_INET, SOCK_STREAM, 0);
  if(tcpsock < 0){
    perror("TCP socket");
    std::cout<<"errno = "<<errno<<std::endl;
    exit(1);
  }
  memset(&tcpAddr, 0, sizeof(tcpAddr));
  tcpAddr.sin_family      = AF_INET;
  tcpAddr.sin_port        = htons(tcpPort);
  tcpAddr.sin_addr.s_addr = inet_addr(sitcpIpAddr);
  std::cout<<"  Done"<<std::endl;

  std::cout<<"  ->Trying to connect to "<<sitcpIpAddr<<" ..."<<std::endl;
  if(connect(tcpsock, (struct sockaddr *)&tcpAddr, sizeof(tcpAddr)) < 0){
    if(errno != EINPROGRESS) perror("TCP connection");
    FD_ZERO(&rmask);
    FD_SET(tcpsock, &rmask);
    wmask = rmask;
    timeout.tv_sec = 3;
    timeout.tv_usec = 0;
    
    int rc = select(tcpsock+1, &rmask, NULL, NULL, &timeout);
    std::cout << "rc : " << rc << std::endl;
    if(rc<0) perror("connect-select error");
    if(rc==0){
      puts("\n     =====time out !=====\n ");
      exit(1);
    }
    else{
      puts("\n     ===connection error===\n ");
      exit(1);
    }
  }  
  FD_ZERO(&readfds);
  FD_SET(tcpsock, &readfds);
  FD_SET(0, &readfds);
 
  puts("  ->Connect success!");

  return true;
}

bool SiTCP::CreateUDPSock(){
  std::cout<<"Create socket for RBCP...";
  udpsock = socket(AF_INET, SOCK_DGRAM, 0);
  if(udpsock < 0){
    perror("UDP socket");
    std::cout<<"errno = "<<errno<<std::endl;
    exit(1);
  }
  memset(&udpAddr, 0, sizeof(udpAddr));
  udpAddr.sin_family      = AF_INET;
  udpAddr.sin_port        = htons(udpPort);
  udpAddr.sin_addr.s_addr = inet_addr(sitcpIpAddr);
  //  inet_aton(sitcpIpAddr, &(udpAddr.sin_addr));   
  std::cout<<"  Done"<<std::endl;
  return true;
}

bool SiTCP::CloseUDPSock(){
  std::cout<<"Close UDP Socket...";
  close(udpsock);
  std::cout<<"  Done"<<std::endl;
  return true;
}

bool SiTCP::CloseTCPSock(){
  std::cout<<"Close TCP Socket...";
  close(tcpsock);
  std::cout<<"  Done"<<std::endl;
  return true;
}

void SiTCP::WriteRBCP(unsigned short address, unsigned short data){
  std::cout << ">>>>>>> " << std::hex << address << "     " << data << std::dec << std::endl;
  
  unsigned char type    = 0xff; //always "0xff"
  unsigned char command = 0x80; //0x80:Write, 0xc0:Read
  unsigned char id      = (unsigned char)address;
  unsigned char length  = 0x01;
  unsigned char tmp_address = (unsigned char)address;
  RBCPskeleton(type, command, id, length, tmp_address);
      
  sndData[0] = (unsigned char)data;
  memcpy(sndBuf+sizeof(GetsndHeader()), sndData, (unsigned short)length);
  this->SendWriteRbcpPackets("WriteRBCP()");

  return;
}

unsigned char SiTCP::ReadRBCP(unsigned short address){

  unsigned char type    = 0xff; //always "0xff"
  unsigned char command = 0xc0; //0x80:Write, 0xc0:Read
  unsigned char id      = (unsigned char)address;
  unsigned char length  = 0x01;
  unsigned char tmp_address = (unsigned char)address;
  RBCPskeleton(type, command, id, length, tmp_address);
      
  memcpy(sndBuf+sizeof(GetsndHeader()), sndData, (unsigned short)length);
  int len = 0;
  len = sendto(GetUDPSock(), sndBuf, length + sizeof(GetsndHeader()), 0,
	       // (struct sockaddr*)&(GetUdpAddr()), sizeof(GetUdpAddr()));
               (struct sockaddr*)&(udpAddr), sizeof(GetUdpAddr()));
  if(len < 0){
    perror("ReadRBCP()");
    exit(1);
  }
  this->rcvRBCPRead_ACK(0);

  return sndData[0];
}

int SiTCP::SendWriteRbcpPackets(char* errmsg){
  int maxTrials = 10;
  bool succeeded = false;
  unsigned short datalength  = (unsigned short)(GetsndHeader()).length;
  unsigned short dataaddress = (unsigned short)(GetsndHeader()).address;
  int cnt(0);
  for(cnt=0; cnt<maxTrials; cnt++)
    {
      int len = 0;
      len = sendto(GetUDPSock(), sndBuf, datalength + sizeof(GetsndHeader()), 0,
		   // (struct sockaddr*)&(GetUdpAddr()), sizeof(GetUdpAddr()));
                   (struct sockaddr*)&(udpAddr), sizeof(GetUdpAddr()));
      if(len < 0){
 	perror(errmsg);
 	exit(1);
      }
      if(rcvRBCPWrite_ACK(0,sndData)){
	succeeded = true;
	break;
      }
    }
  if(not succeeded)
    {
      std::cout << "Failed in writing data to RBCP : Address = " << dataaddress << std::endl;
      exit(1);
    }
//  else
//    {
//      std::cout << "Successfully writing data to RBCP : Address = " << dataaddress << std::endl;
//    }
  return cnt;
}

void SiTCP::RBCPskeleton(unsigned char type, unsigned char command, unsigned char id,
			 unsigned char length, unsigned char address){
  sndHeader.type=type;
  sndHeader.command=command;
  sndHeader.id=id;
  sndHeader.length=length;
  sndHeader.address=htonl(address);
  memcpy(sndBuf, &sndHeader, sizeof(sndHeader));
}

bool SiTCP::rcvRBCPWrite_ACK(int output, unsigned char* data){
  fd_set setSelect;
  int rcvdBytes;
  unsigned char rcvdBuf[1024];
  int recvVal = 1;

  while(recvVal){

    FD_ZERO(&setSelect);
    FD_SET(udpsock, &setSelect);

    timeout.tv_sec  = 0;
    timeout.tv_usec = 120000;
    //    timeout.tv_usec = 500000;

    if(select(udpsock+1, &setSelect, NULL, NULL,&timeout)==0){
      puts("\n***** Timeout ! *****");
      recvVal = 0;
      return false;
    } else {
      /* receive packet */
      if(FD_ISSET(udpsock,&setSelect)){
        rcvdBytes=recvfrom(udpsock, rcvdBuf, 2048, 0, NULL, NULL);
        rcvdBuf[rcvdBytes]=0;
	for(int i=0; i<rcvdBytes; i++){
	  if(i>=8){
	    //checking if the write data is consistent.
	    //	      std::cout<<(unsigned short)rcvdBuf[i]<<" "<<(unsigned short)data[i-8]<<std::endl;
	    if(rcvdBuf[i]!=data[i-8]) return false;
	  }
	}
	if(output){
	  puts("\n***** A pacekt is received ! *****.");
	  puts("Received data:");
	  
	  int j=0;
	  for(int i=0; i<rcvdBytes; i++){
	    if(j==0) {
	      printf("\t[%.3x]:%.2x ",i,rcvdBuf[i]);
	      j++;
	    }else if(j==3){
	      printf("%.2x\n",rcvdBuf[i]);
	      j=0;
	    }else{
	      printf("%.2x ",rcvdBuf[i]);
	      j++;
	    }
	  }
	  puts("\n");
	}
        recvVal = 0;
      }
    }
  }
  return true;
  //usleep(50);
}

bool SiTCP::rcvRBCPRead_ACK(int output){
  fd_set setSelect;
  int rcvdBytes;
  unsigned char rcvdBuf[1024];
  int recvVal = 1;

  while(recvVal){

    FD_ZERO(&setSelect);
    FD_SET(udpsock, &setSelect);

    timeout.tv_sec  = 0;
    timeout.tv_usec = 120000;

    if(select(udpsock+1, &setSelect, NULL, NULL,&timeout)==0){
      puts("\n***** Timeout ! *****");
      recvVal = 0;
      return false;
    } else {
      /* receive packet */
      if(FD_ISSET(udpsock,&setSelect)){
        rcvdBytes=recvfrom(udpsock, rcvdBuf, 2048, 0, NULL, NULL);
        rcvdBuf[rcvdBytes]=0;
	if(rcvdBytes>8) memcpy(sndData, &(rcvdBuf[8]), rcvdBytes-8);
	else return false;
  	if(output){
	  puts("\n***** A pacekt is received ! *****.");
	  puts("Received data:");
	  
	  int j=0;
	  for(int i=0; i<rcvdBytes; i++){
	    if(j==0) {
	      printf("\t[%.3x]:%.2x ",i,rcvdBuf[i]);
	      j++;
	    }else if(j==3){
	      printf("%.2x\n",rcvdBuf[i]);
	      j=0;
	    }else{
	      printf("%.2x ",rcvdBuf[i]);
	      j++;
	    }
	  }
	  puts("\n");
	}
        recvVal = 0;
      }
    }
  }
  return true;
  //usleep(50);
}

int SiTCP::tcpSelect(int usec){
  //return 0 : There is a keyboard input .    
  //return 1 : tcp receiving data ready                          
  //return 2 : timeout
  //return -1 : ERROR         
  memcpy(&fds, &readfds, sizeof(fd_set));
  timeout.tv_sec = 0;
  timeout.tv_usec = usec;
  int n = select(GetTCPSock()+1, &fds, NULL, NULL, &timeout);
  if(n==0)return 2;
  //select(GetTCPSock()+1, &fds, NULL, NULL, NULL);

  if(FD_ISSET(GetTCPSock(), &fds)){
    return 1;
  }else if(FD_ISSET(0, &fds)){
    return 0;
  }else{
    return -1;
  }
}


