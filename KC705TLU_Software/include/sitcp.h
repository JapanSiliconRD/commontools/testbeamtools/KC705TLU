#ifndef SITCP_H
#define SITCP_H
#include "/usr/include/unistd.h"
#include<iostream>
#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<string.h>
#include<arpa/inet.h>

struct bcp_header{
  unsigned char type;
  unsigned char command;
  unsigned char id;
  unsigned char length;
  unsigned int address;
};

class SiTCP{

  char* sitcpIpAddr;
  unsigned int tcpPort;
  unsigned int udpPort;
  int udpsock;
  int tcpsock;

  fd_set rmask, wmask, readfds, fds;
  struct timeval timeout;
  struct bcp_header sndHeader;

 protected:
  struct sockaddr_in tcpAddr;
  struct sockaddr_in udpAddr;

 public:
  SiTCP();
  virtual ~SiTCP();
  unsigned char sndBuf[2048];
  unsigned char sndData[256];
  bool SetIPPort(char* IpAddr, unsigned int tcp, unsigned int udp);
  bool CreateUDPSock();
  bool CloseUDPSock();
  bool CreateTCPSock();
  bool CloseTCPSock();
  
  int GetTCPSock(){return tcpsock;}
  int GetUDPSock(){return udpsock;}

  void          WriteRBCP(unsigned short address, unsigned short data);
  unsigned char ReadRBCP (unsigned short address);

  int SendWriteRbcpPackets(char* errmsg="SendWriteRbcpPackets()");
  int SendReadRbcpPackets(char* errmsg="SendReadRbcpPackets()");
  bool rcvRBCPWrite_ACK(int output, unsigned char* data);
  bool rcvRBCPRead_ACK (int output);
  void RBCPskeleton(unsigned char type, unsigned char command, unsigned char id, 
		    unsigned char length, unsigned char address);
  bcp_header GetsndHeader() const {return sndHeader;}
  const sockaddr_in GetTcpAddr() const {return tcpAddr;}
  const sockaddr_in GetUdpAddr() const {return udpAddr;}

  int tcpSelect(int usec);
  
};
#endif
