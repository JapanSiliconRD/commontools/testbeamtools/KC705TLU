#ifndef _DACCTRL_H_
#define _DACCTRL_H_

//#include <unistd.h>
//#include <stdio.h>
//#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <ctime>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include "TLU_sitcp.h"
#include <iomanip>

#include "TLU_sitcp.h"

#define ADDR_DACTLV_ADD    1
#define ADDR_DACTLV_DATA_L 2
#define ADDR_DACTLV_DATA_U 3

#define ADDR_DACTLV_PRESET  4
#define ADDR_DACTLV_LOAD    5
#define ADDR_DACTLV_WRITE   0
//#define ADDR_DACTLV_SCLKEN  10


void DACTLVWrite( TLU_sitcp *stcp ,int chan) ; 
void DACTLVWriteData( TLU_sitcp *stcp,int chan, int addr, int data ) ; 
void DACTLVLoad( TLU_sitcp *stcp,int chan, int int_data ) ; 
void DACTLVPreset( TLU_sitcp *stcp,int chan, int int_data ) ; 

void DACTLVInit( TLU_sitcp *stcp,int chan ) ; 
void ExecDacCtrlInt(TLU_sitcp *sitcp);
void ExecDacCtrl(TLU_sitcp *sitcp,int chan,int int_cmd,int dacadd=0,int dacval=0);

int getChanOffset(int chan);




#endif
