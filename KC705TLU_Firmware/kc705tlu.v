//-------------------------------------------------------------------//
//
//		Copyright (c) 2012 BeeBeans Technologies
//			All rights reserved
//
//	System      : KC705
//
//	Module      : KC705 Evaluation Board
//
//	Description : Top Module of KC705 Evaluation Board
//
//	file	: KC705 Evaluation Board
//
//	Note	:
//
//
//-------------------------------------------------------------------//

module
	kc705tlu(
	// System
		input wire 	  SYSCLK_200MP_IN , // From 200MHz Oscillator module
		input wire 	  SYSCLK_200MN_IN , // From 200MHz Oscillator module
		input wire 	  SW3,
		input wire 	  SW4,
		input wire 	  SW6,
		output wire [7:0] LED,

        // TLU I/O pins
//		input [7:0] 	  NIMin,
		input [3:0] 	  NIMin,
		output [3:0] 	  NIMout,
//		output [5:0] 	  NIMout,
		input wire [6:0]  LVCMOSin,
		output wire [8:0] LVCMOSout,
		input [1:0] 	  LVDSin,
		output [1:0] 	  LVDSout,
		input [3:0] 	  dout,
		output [3:0] 	  DAC_LDAC,
		output [3:0] 	  DAC_PRE,
		output [3:0] 	  DAC_FS,
		output [3:0] 	  DAC_DIN,
		output [3:0] 	  DAC_SCLK,

		output [3:0] 	  XADC_GPIO,

	// EtherNet
		output wire 	  GMII_RSTn ,
		output wire 	  GMII_TX_EN ,
		output wire [7:0] GMII_TXD ,
		output wire 	  GMII_TX_ER ,
		input wire 	  GMII_TX_CLK ,
		output wire 	  GMII_GTXCLK ,

		input wire 	  GMII_RX_CLK ,
		input wire 	  GMII_RX_DV ,
		input wire [7:0]  GMII_RXD ,
		input wire 	  GMII_RX_ER ,
		input wire 	  GMII_CRS ,
		input wire 	  GMII_COL ,

		inout wire 	  GMII_MDIO ,
		output wire 	  GMII_MDC ,
	// reset switch
		input wire 	  SW_N ,
	
		input wire 	  GPIO_SWITCH_0 ,
		//connect EEPROM
		inout wire 	  I2C_SDA ,
		output wire 	  I2C_SCL
	);


	wire			CLK_200M		;
	IBUFDS #(.IOSTANDARD ("LVDS"))		LVDS_BUF(.O(CLK_200M), .I(SYSCLK_200MP_IN), .IB(SYSCLK_200MN_IN));



   reg 		            RSTn_RUN;
   reg 			    RST_MMCMPLL;
   reg 			    RSTn_TDC;
   reg 			    RSTn_DataHandler;
   reg 			    RSTn_TrigMaker;
   reg 			    RSTn_Emulator;
   reg 			    RSTn_DataSendler;
   reg 			    TrigReady;
   reg 			    orUsrCloseAck;
   reg 			    orUsrTxWe;
   reg [7:0] 		    orUsrTxWd;
   reg 			    irUsrActive;
   reg 			    irUsrCloseReq;
   reg 			    irUsrTxAfull;
   reg 			    irUsrRxEmpty;
   reg 			    irUsrRxRv;
   reg [7:0] 		    irUsrRxRd;

   
   reg [87:0] 		    dataoutcopy;
   reg 			    dataoutseri;
   reg [7:0] 		    dataoutcnt;
			    
			    
   reg [7:0] 		    commandincopy;
   reg 			    commandinseri;
   reg [3:0]		    commandincnt;
	   
   reg [4:0] 		    FIFODelayCNT;
   reg [3:0] 		    DatawidthCNT;

   reg [3:0] 		    REG_DAC_ADD;
   reg [3:0] 		    REG_DAC_VAL1;
   reg [3:0] 		    REG_DAC_VAL2;
   reg [3:0] 		    REG_DAC_VAL3;
   
   reg 			    Dac_ConfigEn;
   reg 			    Dac_WriteEn;
   reg [3:0] 		    Dac_Add;
   reg [11:0] 		    Dac_Data;


   wire [3:0] 		    dac_ldac_s;
   wire [3:0] 		    dac_pre_s;
   wire [3:0] 		    dac_fs_s;
   wire [3:0] 		    dac_din_s;
   wire [3:0] 		    dac_sclk_s;
   

   
   reg [7:0] 		    debugled;
   
   wire 		    CLK100K;
   wire [15:0] 		    TriggerCosmicID;
   wire [15:0] 		    TriggerID;
   wire [15:0] 		    TriggerID2;
   wire [31:0] 		    TDCcount;
   wire [87:0] 		    DataOUT;
   
   wire [3:0] 		    EMUROIin;
   wire [3:0] 		    EMUNIMin;
   wire [3:0] 		    EMUBusyIn;
   wire 		    DataValid;
   wire [7:0] 		    ReadData;
   wire [23:0]		    TimeStamp;
   wire 		    TrigBeam;
   wire 		    TrigBeam2;
   wire 		    TrigCosmic;
   wire 		    TrigStoppedmu;
   wire 		    TrigDecayEle;
   wire 		    ROIBit;
   wire                     BusyOut;
   wire [3:0]		    TrigBit;
   wire [3:0]		    TrigBit2;
   wire 		    TrigCosmicLEDOUT;
   wire 		    TrigStoppedmuLEDOUT;
   wire 		    TrigDecayEleLEDOUT;


   initial begin
      RST_MMCMPLL<=1'b0;
      RSTn_TDC<=1'b1;
      RSTn_DataHandler<=1'b1;
      RSTn_Emulator<=1'b1;
      RSTn_TrigMaker<=1'b1;
//      RSTn_TrigMaker<=SW[0];
      RSTn_DataSendler<=1'b1;
//    This must be 1'b0 if remote trigger activation used
      RSTn_RUN<=1'b0;
      TrigReady<=1'b1;
      orUsrCloseAck<=1'b0;
      orUsrTxWe<=1'b0;
      orUsrTxWd<=8'b0;
      irUsrActive<=1'b0;
      irUsrCloseReq<=1'b0;
      irUsrTxAfull<=1'b0;
      dataoutcopy<=88'b0;
      dataoutseri<=1'b0;
      dataoutcnt<=7'b0;
      commandincopy<=8'b0;
      commandinseri<=1'b0;
      commandincnt<=4'b0;
      Dac_ConfigEn<=1'b0;
      Dac_WriteEn<=1'b0;
      Dac_Add<=4'b0;
      Dac_Data<=12'd2370;

      REG_DAC_ADD<=4'b0;
      REG_DAC_VAL1<=4'b0;
      REG_DAC_VAL2<=4'b0;
      REG_DAC_VAL3<=4'b0;
      debugled<=8'b0;


   end

   clk_wiz_0 clk_w(
      // Clock out ports
      .clk_out1(clk_20M_s),
      .clk_out2(clk_40M_s),
      .clk_out3(CLK50M),
      .clk_out4(clk_100M_s),
      .clk_out5(clk_160M_s),
      .clk_out6(clk_400M_s),
      // Status and control signals
      .reset(RST_MMCMPLL),
      .locked(dcmpll_locked_s),
     // Clock in port
	.clk_in1(CLK_200M)
    );
   
   TLU_TrigEmulator emu(.CLK0(clk_40M_s),
//			.RSTn(RSTn_Emulator),
//			.RSTn(!SW[0]&RSTn_RUN),
			.RSTn(RSTn_RUN),
//			.RSTn(!SW[0]),
			.EMUROIin(EMUROIin),
			.EMUNIMin(EMUNIMin),
			.EMUBusyIn(EMUBusyIn)
			);
   
   
   TLU_TrigMaker tlutm(.CLK0(clk_100M_s),
//		       .RSTn(RSTn_TrigMaker),
//		       .RSTn(!SW[0]&RSTn_RUN),
		       .RSTn(RSTn_RUN),
//		       .RSTn(!SW[0]),
//  	       .Debug(~SW[1]),
//		       .Debug(SW[1]),
//		       .Debug(1'b1),
				 .Debug(1'b0),
             .TrigReady(TrigReady),
///////////////////////////////////////////////////////////////
////    For Emulation 
		       .dout(EMUNIMin),  // Emulated nim signal		
		       .BusyIn(EMUBusyIn),  // Emulated busy signal		
//	       .BusyIn(3'b0),  // Emulated busy signal		
//		       .ROIin(EMUROIin),  // Emulated ROI signal
		       .ROIin(EMUNIMin),  // Emulated ROI signal
/////////////////////////////////////////////////////
//     For One scintilator + HSIO2 + FlashADC + Lecroy   ////2021 30 June
//		       .dout({dout[0],dout[0],dout[0],dout[0]}),   //trigger from MPPC0
//		       .BusyIn({LVCMOSin[0],LVCMOSin[0],LVCMOSin[0]}),  // Emulated busy signal
//		       .ROIin(NIMin[0]),  // Emulated ROI signal
/////////////////////////////////////////////////////
//     For Two scintilator + HSIO2 + FlashADC + Lecroy   ////2021 30 June
//		       .dout({dout[0],dout[1],dout[1],dout[1]}),   //trigger from MPPC0
//		       .BusyIn({LVCMOSin[0],LVCMOSin[0],LVCMOSin[0]}),  // Emulated busy signal
//		       .ROIin(NIMin[0]),  // Emulated ROI signal
/////////////////////////////////////////////////////
//     For One scintilator test   ////2020 Feb 17
//		       .dout({dout[0],dout[0],dout[0],dout[0]}),   //trigger from MPPC0
//		       .BusyIn(EMUBusyIn),  // Emulated busy signal
//		       .ROIin(EMUROIin),  // Emulated ROI signal
///////////////////////////////////////////////////////////////////
//    For Test
//		       .NIMin({NIMin[7],NIMin[7],NIMin[7],NIMin[7]}), // 
//				 .BusyIn(EMUBusyIn),  // Emulated busy signal		
//		       .BusyIn({LVCMOSin[0],LVCMOSin[0],LVCMOSin[0]}),
//		       .ROIin(~NIMin[0]),
//		       .ROIin(NIMin[7]),
//		       .ROIin(NIMin[7]),
///////////////////////////////////////////////////////////////////
////    For Actual Trigger
//		       .dout(dout), 
//////				 .BusyIn({LVCMOSin[0],NIMin[6],LVDSin[0]}), //Full Busy
//////				 .BusyIn({LVCMOSin[0],LVCMOSin[0],LVDSin[0]}), // no DRS4
////				 .BusyIn({LVCMOSin[0],LVCMOSin[0],NIMin[6]}), //no K7
//				 .BusyIn({LVCMOSin[0],LVCMOSin[0],LVCMOSin[0]}), //HSIO2 busy only
//				 .ROIin(NIMin[7]),
//		       .ROIin(1'b1),
//			 
		       .CLK100K(CLK100K),
		       .clk_20M_s(clk_20M_s),
		       .TrigBeam(TrigBeam),
		       .TimeStamp(TimeStamp),
		       .TrigCosmic(TrigCosmic),
		       .TrigStoppedmu(TrigStoppedmu),
		       .TrigDecayEle(TrigDecayEle),
		       .ROIBit(ROIBit),
		       .BusyOut(BusyOut),
		       .TrigBit(TrigBit),
		       .TriggerID(TriggerID),
		       .TriggerCosmicID(TriggerCosmicID)
		       );

   TLU_TDC tlutdc(.CLK0(clk_100M_s),
//		  .RSTn(RSTn_TDC),
//		  .RSTn(!SW[0]&RSTn_RUN),
		  .RSTn(RSTn_RUN),
//		  .RSTn(!SW[0]),
		  .start(TrigStoppedmu),
		  .stop(TrigDecayEle),
		  .MAXCOUNT(32'd100000), // 100k x 10us = 1ms 
		  .TDCcount(TDCcount),
		  .GATEOUT(GATEOUT)
		  );

   TLU_DataHandler tludh(.CLK0(clk_100M_s),
//			 .RSTn(RSTn_DataHandler),
//			 .RSTn(!SW[0]&RSTn_RUN),
			 .RSTn(RSTn_RUN),
//			 .RSTn(!SW[0]),
			 .TriggerID(TriggerID),
			 .TrigBit(TrigBit),
			 .TimeStamp(TimeStamp),
			 .TrigBeam(TrigBeam),
			 .TrigCosmic(TrigCosmic),
			 .TrigStoppedmu(TrigStoppedmu),
			 .TrigDecayEle(TrigDecayEle),
			 .TDCcount(TDCcount[23:0]),
			 .GATEOUT(GATEOUT),
			 .DataOUT(DataOUT),
			 .DataValid(DataValid)
			 );
   
   TLU_DataSender tluds(.CLK0(CLK_200M),
//			.RSTn(RSTn_DataSendler),
//			.RSTn(!SW[0]&RSTn_RUN),
			.RSTn(RSTn_RUN),
//			.RSTn(!SW[0]),
			.DataIn(DataOUT),
			.DataValid(DataValid),
			.ReadValid(ReadValid),
			.DataSenderReady(DataSenderReady),
			.ReadData(ReadData)
//			.ReadData(TCP_RX_DATA)
			);
   
   
   TLU_ShowLED tluled(.CLK0(clk_100M_s),
		      .TrigCosmic(TrigCosmic),
		      .TrigStoppedmu(TrigStoppedmu),
		      .TrigDecayEle(TrigDecayEle),
		      .TrigCosmicLEDOUT(TrigCosmicLEDOUT),
		      .TrigStoppedmuLEDOUT(TrigStoppedmuLEDOUT),
		      .TrigDecayEleLEDOUT(TrigDecayEleLEDOUT)
		      );



   // making 100K TimeStamp CLK
   parameter [9:0] CLKREDUCTION=10'd499;
   reg 			     CLK100Kr;
   reg [9:0] 		     CLK100KCNT;
   initial begin
      CLK100KCNT<=10'b0;
      CLK100Kr<=1'b1;
   end
   always@ (posedge clk_100M_s) begin
      if(CLK100KCNT!=CLKREDUCTION) 
	CLK100KCNT<=CLK100KCNT + 10'b1;
      else begin
	 CLK100KCNT<=10'b0000;
	 //	 if(CLK100Kr==1'b1 & RSTn_RUN)
	 if(CLK100Kr==1'b1)
	   CLK100Kr <= 1'b0;
	 else begin
	    CLK100Kr <= 1'b1;
	 end
      end
   end
   
   
   
   
   assign CLK100K=CLK100Kr;

  assign LVCMOSout[0] = EMUNIMin;
//   assign LVCMOSout[0] = clk_100M_s; /// To HSIO2
//   assign LVCMOSout[1] = LVCMOSin[0]; //HSIO2 busy to  pico B
   assign LVCMOSout[1] = TrigBeam; //HSIO2 busy to  pico B    
   assign LVCMOSout[2] = NIMin[0]; // ROI   to pico C 
   assign LVCMOSout[3] = EMUBusyIn; // 
 //  assign LVCMOSout[4] = TrigBeam;
   assign LVCMOSout[4] = clk_40M_s;
   assign LVCMOSout[5] = TrigBeam;
   assign LVCMOSout[6] = (SW3==1'b1)? 1'b1:1'b0;
   assign LVCMOSout[7] = (SW4==1'b1)? 1'b1:1'b0;
   assign LVCMOSout[8] = (SW6==1'b1)? 1'b1:1'b0;
   assign LVCMOSout[9] = (SW6==1'b1)? 1'b1:1'b0;

   assign NIMout[0] = TrigBeam;
   assign NIMout[1] = TrigBeam;
   assign NIMout[2] = NIMin[0];
   assign NIMout[3] = TrigBeam;
   
   assign LED[0] = debugled[0];
   assign LED[1] = debugled[1];
   assign LED[2] = debugled[2];
   assign LED[3] = debugled[3];
   assign LED[4] = debugled[4];
   assign LED[5] = debugled[5];
   assign LED[6] = debugled[6];
   assign LED[7] = debugled[7];
 //  assign LED[7] = clk_40M_s;
   
   assign XADC_GPIO[0] = EMUNIMin[0];
   assign XADC_GPIO[1] = clk_40M_s;
   assign XADC_GPIO[2] = TrigBeam;
   assign XADC_GPIO[3] = clk_40M_s;
   

   


   


/////////////////////////////////////////////////////
// SiTCP code
   
//------------------------------------------------------------------------------
//	Buffers
//------------------------------------------------------------------------------
	wire			GMII_MDIO_IN	;
	wire			GMII_MDIO_OE	;
	wire			GMII_MDIO_OUT	;
	wire			CLK_125M		;
	wire			LOCKED			;
	wire			PLL_CLKFB		;
	wire			BUF_TX_CLK		;
	wire			SiTCP_RST		;
	wire			TCP_OPEN_ACK	;
	wire			TCP_CLOSE_REQ	;
	wire			TCP_RX_WR		;
	wire	[7:0]	TCP_RX_DATA		;
	wire			TCP_TX_FULL		;
	wire	[31:0]	RBCP_ADDR		;  // was SIO_ADDR in Seabas
	wire	[7:0]	RBCP_WD			;
	wire			RBCP_WE			;
	wire			RBCP_RE			;
	reg				TCP_CLOSE_ACK	;
	wire	[7:0]	TCP_TX_DATA		;
	reg				RBCP_ACK		;
	reg		[7:0]	RBCP_RD			;
	reg		[31:0]	OFFSET_TEST		;
	wire	[11:0]	FIFO_DATA_COUNT	;
	wire			FIFO_RD_VALID	;
	reg				SYS_RSTn		;
	reg		[29:0]	INICNT			;
	reg				GMII_1000M;
	reg		[8:0]	CNT_CLK;
	reg				CNT_RST;
	reg				CNT_LD;
	reg		[6:0]	RX_CNT;




//   reg 			[7:0]	ReadDatar;
   








   


	PLLE2_BASE #(
		.CLKFBOUT_MULT			(5),
		.CLKIN1_PERIOD			(5.000),
		.CLKOUT0_DIVIDE			(8),
		.CLKOUT0_DUTY_CYCLE		(0.500),
		.DIVCLK_DIVIDE			(1)
	) 
	PLLE2_BASE(
		.CLKFBOUT				(PLL_CLKFB),
		.CLKOUT0				(CLK_125M),
		.CLKOUT1				(),
		.CLKOUT2				(),
		.CLKOUT3				(),
		.CLKOUT4				(),
		.CLKOUT5				(),
		.LOCKED					(LOCKED),
		.CLKFBIN				(PLL_CLKFB),
		.CLKIN1					(CLK_200M),
		.PWRDWN					(1'b0),
		.RST					(1'b0)
	);


	//SYS_RSTn->off//
	always@(posedge CLK_200M or negedge LOCKED)begin
		if (SW_N || (LOCKED == 1'b0)) begin
			INICNT[29:0]	<=	30'd0;
			SYS_RSTn		<= 1'b0;
		end else begin
			INICNT[29:0]	<=	INICNT[29]? INICNT[29:0]:	(INICNT[29:0] + 30'd1);
			SYS_RSTn		<=	INICNT[29];
		end
	end

	wire			EEPROM_CS;
	wire			EEPROM_SK;
	wire			EEPROM_DI;
	wire			EEPROM_DO;


	AT93C46_IIC #(
		.PCA9548_AD			(7'b1110_100),				// PCA9548 Dvice Address
		.PCA9548_SL			(8'b0000_1000),				// PCA9548 Select code (Ch3,Ch4 enable)
		.IIC_MEM_AD			(7'b1010_100),				// IIC Memory Dvice Address
		.FREQUENCY			(8'd200),					// CLK_IN Frequency  > 10MHz
		.DRIVE				(4),						// Output Buffer Strength
		.IOSTANDARD			("LVCMOS25"),				// I/O Standard
		.SLEW				("SLOW")					// Outputbufer Slew rate
	)
	AT93C46_IIC(
		.CLK_IN				(CLK_200M),					// System Clock
		.RESET_IN			(~SYS_RSTn),				// Reset
		.IIC_INIT_OUT		(RST_EEPROM),				// IIC , AT93C46 Initialize (0=Initialize End)
		.EEPROM_CS_IN		(EEPROM_CS),				// AT93C46 Chip select
		.EEPROM_SK_IN		(EEPROM_SK),				// AT93C46 Serial data clock
		.EEPROM_DI_IN		(EEPROM_DI),				// AT93C46 Serial write data (Master to Memory)
		.EEPROM_DO_OUT		(EEPROM_DO),				// AT93C46 Serial read data(Slave to Master)
		.INIT_ERR_OUT		(),							// PCA9548 Initialize Error
		.IIC_REQ_IN			(1'b0),						// IIC ch0 Request
		.IIC_NUM_IN			(8'h00),					// IIC ch0 Number of Access[7:0]	0x00:1Byte , 0xff:256Byte
		.IIC_DAD_IN			(7'b0),						// IIC ch0 Device Address[6:0]
		.IIC_ADR_IN			(8'b0),						// IIC ch0 Word Address[7:0]
		.IIC_RNW_IN			(1'b0),						// IIC ch0 Read(1) / Write(0)
		.IIC_WDT_IN			(8'b0),						// IIC ch0 Write Data[7:0]
		.IIC_RAK_OUT		(),							// IIC ch0 Request Acknowledge
		.IIC_WDA_OUT		(),							// IIC ch0 Wite Data Acknowledge(Next Data Request)
		.IIC_WAE_OUT		(),							// IIC ch0 Wite Last Data Acknowledge(same as IIC_WDA timing)
		.IIC_BSY_OUT		(),							// IIC ch0 Busy
		.IIC_RDT_OUT		(),							// IIC ch0 Read Data[7:0]
		.IIC_RVL_OUT		(),							// IIC ch0 Read Data Valid
		.IIC_EOR_OUT		(),							// IIC ch0 End of Read Data(same as IIC_RVL timing)
		.IIC_ERR_OUT		(),							// IIC ch0 Error Detect
		// Device Interface
		.IIC_SCL_OUT		(I2C_SCL),					// IIC Clock
		.IIC_SDA_IO			(I2C_SDA)					// IIC Data
	);


	BUFGMUX GMIIMUX(.O(BUF_TX_CLK), .I0(GMII_TX_CLK), .I1(CLK_125M), .S(GMII_1000M));
	ODDR	IOB_GTX		(.C(BUF_TX_CLK), .CE(1'b1), .D1(1'b1), .D2(1'b0), .R(1'b0), .S(1'b0), .Q(GMII_GTXCLK));


	always@(posedge CLK_200M or negedge SYS_RSTn)begin
		if (~SYS_RSTn) begin
			CNT_CLK[8:0]	<=	9'b0;
			CNT_LD			<=	1'b0;
			CNT_RST			<=	1'b1;
			GMII_1000M		<=	1'b0;
		end else begin 
			CNT_CLK[8:0]	<=	CNT_CLK[8] ? 9'd198 : CNT_CLK[8:0] - 9'd1;
			CNT_LD			<=	CNT_CLK[8];
			CNT_RST			<=	CNT_LD;
			GMII_1000M		<=	CNT_LD ? RX_CNT[6] : GMII_1000M;
		end
	end
	
	always@(posedge GMII_RX_CLK or posedge CNT_RST)begin
		if (CNT_RST) begin
			RX_CNT[6:0]		<=	7'd0;
		end else begin
			RX_CNT[6:0]		<=	RX_CNT[6] ? RX_CNT[6:0] : RX_CNT[6:0] + 7'd1;
		end
	end


	assign	GMII_MDIO	= (GMII_MDIO_OE	?	GMII_MDIO_OUT : 1'bz)	;


	WRAP_SiTCP_GMII_XC7K_32K	#(
		.TIM_PERIOD			(8'd200)					// = System clock frequency(MHz), integer only
	)
	SiTCP	(
		.CLK				(CLK_200M),					// in	: System Clock (MII: >15MHz, GMII>129MHz)
		.RST				(RST_EEPROM),				// in	: System reset
	// Configuration parameters
		.FORCE_DEFAULTn		(1'b0),						// in	: Load default parameters
		.EXT_IP_ADDR		(32'h0000_0000),			// in	: IP address[31:0]
		.EXT_TCP_PORT		(16'h0000),					// in	: TCP port #[15:0]
		.EXT_RBCP_PORT		(16'h0000),					// in	: RBCP port #[15:0]
		.PHY_ADDR			(5'b0_0111),				// in	: PHY-device MIF address[4:0]
	// EEPROM
		.EEPROM_CS			(EEPROM_CS	),				// out	: Chip select
		.EEPROM_SK			(EEPROM_SK	),				// out	: Serial data clock
		.EEPROM_DI			(EEPROM_DI	),				// out	: Serial write data
		.EEPROM_DO			(EEPROM_DO	),				// in	: Serial read data
	// user data, intialial values are stored in the EEPROM, 0xFFFF_FC3C-3F
		.USR_REG_X3C		(),							// out	: Stored at 0xFFFF_FF3C
		.USR_REG_X3D		(),							// out	: Stored at 0xFFFF_FF3D
		.USR_REG_X3E		(),							// out	: Stored at 0xFFFF_FF3E
		.USR_REG_X3F		(),							// out	: Stored at 0xFFFF_FF3F
	// MII interface
		.GMII_RSTn			(GMII_RSTn),				// out	: PHY reset
		.GMII_1000M			(GMII_1000M),				// in	: GMII mode (0:MII, 1:GMII)
		// TX
		.GMII_TX_CLK		(BUF_TX_CLK),				// in	: Tx clock
		.GMII_TX_EN			(GMII_TX_EN),				// out	: Tx enable
		.GMII_TXD			(GMII_TXD[7:0]),			// out	: Tx data[7:0]
		.GMII_TX_ER			(GMII_TX_ER),				// out	: TX error
		// RX
		.GMII_RX_CLK		(GMII_RX_CLK),				// in	: Rx clock
		.GMII_RX_DV			(GMII_RX_DV),				// in	: Rx data valid
		.GMII_RXD			(GMII_RXD[7:0]),			// in	: Rx data[7:0]
		.GMII_RX_ER			(GMII_RX_ER),				// in	: Rx error
		.GMII_CRS			(GMII_CRS),					// in	: Carrier sense
		.GMII_COL			(GMII_COL),					// in	: Collision detected
		// Management IF
		.GMII_MDC			(GMII_MDC),					// out	: Clock for MDIO
		.GMII_MDIO_IN		(GMII_MDIO),				// in	: Data
		.GMII_MDIO_OUT		(GMII_MDIO_OUT),			// out	: Data
		.GMII_MDIO_OE		(GMII_MDIO_OE),				// out	: MDIO output enable
	// User I/F
		.SiTCP_RST			(SiTCP_RST),				// out	: Reset for SiTCP and related circuits
		// TCP connection control
		.TCP_OPEN_REQ		(1'b0),						// in	: Reserved input, shoud be 0
		.TCP_OPEN_ACK		(TCP_OPEN_ACK),				// out	: Acknowledge for open (=Socket busy)
		.TCP_ERROR			(),							// out	: TCP error, its active period is equal to MSL
		.TCP_CLOSE_REQ		(TCP_CLOSE_REQ),			// out	: Connection close request
		.TCP_CLOSE_ACK		(TCP_CLOSE_REQ),			// in	: Acknowledge for closing
		// FIFO I/F
		.TCP_RX_WC			({4'b1111,FIFO_DATA_COUNT[11:0]}),					// in	: Rx FIFO write count[15:0] (Unused bits should be set 1)
		.TCP_RX_WR			(TCP_RX_WR),				// out	: Write enable
		.TCP_RX_DATA		(TCP_RX_DATA[7:0]),			// out	: Write data[7:0]
//		.TCP_TX_FULL		(TCP_TX_FULL),				// out	: Almost full flag
//		.TCP_TX_WR			(FIFO_RD_VALID),			// in	: Write enable
//		.TCP_TX_DATA		(TCP_TX_DATA[7:0]),			// in	: Write data[7:0]
		.TCP_TX_FULL		(DataSenderReady),				// out	: Almost full flag
		.TCP_TX_WR			(ReadValid),			// in	: Write enable
		.TCP_TX_DATA		(ReadData),			// in	: Write data[7:0]
	// RBCP
		.RBCP_ACT			(		),					// out	: RBCP active
		.RBCP_ADDR			(RBCP_ADDR[31:0]),			// out	: Address[31:0]
		.RBCP_WD			(RBCP_WD[7:0]),				// out	: Data[7:0]
		.RBCP_WE			(RBCP_WE),					// out	: Write enable
		.RBCP_RE			(RBCP_RE),					// out	: Read enable
		.RBCP_ACK			(RBCP_ACK),					// in	: Access acknowledge
		.RBCP_RD			(RBCP_RD[7:0])				// in	: Read data[7:0]
	);


//FIFO
//	fifo_generator_v11_0 fifo_generator_v11_0(
//	  .clk			(CLK_200M				),//in	:
//	  .rst			(~TCP_OPEN_ACK			),//in	:
////	  .din			(TCP_RX_DATA[7:0]		),//in	:
//	  .din			(ReadData[7:0]		),//in	:
//	  .wr_en		(TCP_RX_WR				),//in	:
//	  .full			(						),//out	:
//	  .dout			(TCP_TX_DATA[7:0]		),//out	:
//	  .valid		(FIFO_RD_VALID			),//out	:active hi
//	  .rd_en		(~TCP_TX_FULL			),//in	:
//	  .empty		(						),//out	:
//	  .data_count	(FIFO_DATA_COUNT[11:0]	)//out	:[11:0]
//	);


//RBCP_test
//	always@(posedge CLK_200M)begin
//		if(RBCP_WE)begin
//			OFFSET_TEST[31:0]  <= {RBCP_ADDR[31:2],2'b00}+{RBCP_WD[7:0],RBCP_WD[7:0],RBCP_WD[7:0],RBCP_WD[7:0]};
//		end
//		RBCP_RD[7:0]	<=  (
//			((RBCP_ADDR[7:0]==8'h00)?	OFFSET_TEST[ 7: 0]:	8'h00)|
//			((RBCP_ADDR[7:0]==8'h01)?	OFFSET_TEST[15: 8]:	8'h00)|
//			((RBCP_ADDR[7:0]==8'h02)?	OFFSET_TEST[23:16]:	8'h00)|
//			((RBCP_ADDR[7:0]==8'h03)?	OFFSET_TEST[31:24]:	8'h00)
//		);
//		RBCP_ACK  <= RBCP_RE | RBCP_WE;
//	end



   //---------- Buffer ----------
   // UDP reg
   reg [7:0] 		    x00_reg;
   reg [7:0] 		    x01_reg;
   reg [7:0] 		    x02_reg;
   reg [7:0] 		    x03_reg;
   reg [7:0] 		    x04_reg;
   reg [7:0] 		    x05_reg;
   reg [7:0] 		    x0A_reg;
   reg [7:0] 		    x0B_reg;
   reg [7:0] 		    x0C_reg;
   reg [7:0] 		    x0D_reg;
   reg [7:0] 		    x0E_reg;
   reg [7:0] 		    x0F_reg;
   reg [7:0] 		    x10_reg;
   reg [7:0] 		    x11_reg;
   reg [7:0] 		    x12_reg;
   reg [7:0] 		    x13_reg;
   reg [7:0] 		    x14_reg;
   reg [7:0] 		    x15_reg;
   reg [7:0] 		    x1A_reg;
   reg [7:0] 		    x1B_reg;
   reg [7:0] 		    x1C_reg;
   reg [7:0] 		    x1D_reg;
   reg [7:0] 		    x1E_reg;
   reg [7:0] 		    x1F_reg;


    wire  [15:0] DATA_IN0 ;
    wire  [15:0] DATA_IN1 ;
    wire  [15:0] DATA_IN2 ;
    wire  [15:0] DATA_IN3 ;
    wire  [3:0]  DAC_ADDR0 ;
    wire  [3:0]  DAC_ADDR1 ;
    wire  [3:0]  DAC_ADDR2 ;
    wire  [3:0]  DAC_ADDR3 ;
    wire  [11:0] DAC_DATA0 ;
    wire  [11:0] DAC_DATA1 ;
    wire  [11:0] DAC_DATA2 ;
    wire  [11:0] DAC_DATA3 ;
    wire  [3:0]       DAC_IN_START ;
    wire  [3:0]       BUSY ;

   wire [3:0] 	 PRE ;
   wire [3:0] 	 LDAC ;
   wire [3:0] 	 SCLK ;
   wire [3:0] 	 DIN ;
   wire [3:0] 	 FS ;


   
   


   // UDP command treatment
   parameter [7:0] STARTRUNENABLE=8'b11111010;  // 8'hfa
   parameter [7:0] STOPRUNENABLE=8'b11110101; // 8'hf5
   parameter [7:0] CONFIGDAC=8'b11110001;   // 8'hf1
   parameter [7:0] DACLVON=8'b11110010;  // 8'hf2
   
   /////////////////////////////////
   // remote start
   /////////////////////////////////
   wire 		    regCs ;
   assign       regCs   = (RBCP_ADDR[31:8]==24'd0);
   always@ (posedge clk_100M_s) begin
//   always@ (posedge CLK_200M) begin
//      if(RBCP_WE)begin
	    x00_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h00) ? RBCP_WD[7:0] : x00_reg[7:0]);
	    x01_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h01) ? RBCP_WD[7:0] : x01_reg[7:0]);
	    x02_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h02) ? RBCP_WD[7:0] : x02_reg[7:0]);
	    x03_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h03) ? RBCP_WD[7:0] : x03_reg[7:0]);
	    x04_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h04) ? RBCP_WD[7:0] : x04_reg[7:0]);
	    x05_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h05) ? RBCP_WD[7:0] : x05_reg[7:0]);
	    x0A_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0A) ? RBCP_WD[7:0] : x0A_reg[7:0]);
	    x0B_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0B) ? RBCP_WD[7:0] : x0B_reg[7:0]);
	    x0C_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0C) ? RBCP_WD[7:0] : x0C_reg[7:0]);
	    x0D_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0D) ? RBCP_WD[7:0] : x0D_reg[7:0]);
	    x0E_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0E) ? RBCP_WD[7:0] : x0E_reg[7:0]);
	    x0F_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h0F) ? RBCP_WD[7:0] : x0F_reg[7:0]);
	    x10_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h10) ? RBCP_WD[7:0] : x10_reg[7:0]);
	    x11_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h11) ? RBCP_WD[7:0] : x11_reg[7:0]);
	    x12_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h12) ? RBCP_WD[7:0] : x12_reg[7:0]);
	    x13_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h13) ? RBCP_WD[7:0] : x13_reg[7:0]);
	    x14_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h14) ? RBCP_WD[7:0] : x14_reg[7:0]);
	    x15_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h15) ? RBCP_WD[7:0] : x15_reg[7:0]);
	    x1A_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1A) ? RBCP_WD[7:0] : x1A_reg[7:0]);
	    x1B_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1B) ? RBCP_WD[7:0] : x1B_reg[7:0]);
	    x1C_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1C) ? RBCP_WD[7:0] : x1C_reg[7:0]);
	    x1D_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1D) ? RBCP_WD[7:0] : x1D_reg[7:0]);
	    x1E_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1E) ? RBCP_WD[7:0] : x1E_reg[7:0]);
	    x1F_reg[7:0]        <= (regCs & (RBCP_ADDR[7:0]==8'h1F) ? RBCP_WD[7:0] : x1F_reg[7:0]);

      
//      end
      if(x00_reg==STARTRUNENABLE)begin
	 RSTn_RUN<=1'b1;
      end
      if(x00_reg==STOPRUNENABLE) begin
	 RSTn_RUN<=1'b0;
      end
      if(x00_reg!=8'b0)begin
	 debugled<=x00_reg;	    
      end
//      debugled<=x00_reg;
      RBCP_ACK  <= RBCP_RE | RBCP_WE;
//      if(RBCP_WD==STARTRUNENABLE)begin
//	 RSTn_RUN<=1'b1;
//	 debugled<=RBCP_WD[7:0];	    
//      end
//      if(RBCP_WD==STOPRUNENABLE) begin
//	 RSTn_RUN<=1'b0;
//	 debugled<=RBCP_WD[7:0];	    
//	 
//      end
   end



    assign  DAC_IN_START[0]   = x00_reg[0] ;
    assign  DAC_IN_START[1]   = x0A_reg[0] ;
    assign  DAC_IN_START[2]   = x10_reg[0] ;
    assign  DAC_IN_START[3]   = x1A_reg[0] ;

    assign  DAC_ADDR0[3:0]  = x01_reg[3:0] ;
    assign  DAC_ADDR1[3:0]  = x0B_reg[3:0] ;
    assign  DAC_ADDR2[3:0]  = x11_reg[3:0] ;
    assign  DAC_ADDR3[3:0]  = x1B_reg[3:0] ;
   
    assign  DAC_DATA0[7:0]  = x02_reg[7:0] ;
    assign  DAC_DATA1[7:0]  = x0C_reg[7:0] ;
    assign  DAC_DATA2[7:0]  = x12_reg[7:0] ;
    assign  DAC_DATA3[7:0]  = x1C_reg[7:0] ;

    assign  DAC_DATA0[11:8] = x03_reg[3:0] ;
    assign  DAC_DATA1[11:8] = x0D_reg[3:0] ;
    assign  DAC_DATA2[11:8] = x13_reg[3:0] ;
    assign  DAC_DATA3[11:8] = x1D_reg[3:0] ;

    assign  DATA_IN0        = { DAC_ADDR0[3:0], DAC_DATA0[11:0] } ;
    assign  DATA_IN1        = { DAC_ADDR1[3:0], DAC_DATA1[11:0] } ;
    assign  DATA_IN2        = { DAC_ADDR2[3:0], DAC_DATA2[11:0] } ;
    assign  DATA_IN3        = { DAC_ADDR3[3:0], DAC_DATA3[11:0] } ;
    
    assign  PRE[0]            = x04_reg[0] ;
    assign  PRE[1]            = x0E_reg[0] ;
    assign  PRE[2]            = x14_reg[0] ;
    assign  PRE[3]            = x1E_reg[0] ;

    assign  LDAC[0]           = x05_reg[0] ;
    assign  LDAC[1]           = x0F_reg[0] ;
    assign  LDAC[2]           = x15_reg[0] ;
    assign  LDAC[3]           = x1F_reg[0] ;


   
    TLU_DacCtl tludac (
                        .CLK(clk_20M_s),
//                        .NRST_X(!SW[0]),
                        .NRST_X(1'b1),
                        .DATA_IN0(DATA_IN0),
                        .DATA_IN1(DATA_IN1),
                        .DATA_IN2(DATA_IN2),
                        .DATA_IN3(DATA_IN3),
                        .DAC_IN_START(DAC_IN_START),
                        .SCLK(SCLK),
                        .DIN(DIN),
                        .FS(FS),
                        .BUSY(BUSY)
    ) ;


   assign       DAC_LDAC[0]=~LDAC[0];
   assign       DAC_LDAC[1]=~LDAC[1];
   assign       DAC_LDAC[2]=~LDAC[2];
   assign       DAC_LDAC[3]=~LDAC[3];
   assign	DAC_PRE[0]=~PRE[0];
   assign	DAC_PRE[1]=~PRE[1];
   assign	DAC_PRE[2]=~PRE[2];
   assign	DAC_PRE[3]=~PRE[3];
   assign	DAC_FS[0]=FS[0];
   assign	DAC_FS[1]=FS[1];
   assign	DAC_FS[2]=FS[2];
   assign	DAC_FS[3]=FS[3];
   assign	DAC_DIN[0]=DIN[0];
   assign	DAC_DIN[1]=DIN[1];
   assign	DAC_DIN[2]=DIN[2];
   assign	DAC_DIN[3]=DIN[3];
   assign	DAC_SCLK[0]=SCLK[0];
   assign	DAC_SCLK[1]=SCLK[1];
   assign	DAC_SCLK[2]=SCLK[2];
   assign	DAC_SCLK[3]=SCLK[3];






   


endmodule
