`timescale 1ns / 1ps
`define DELAY_UNIT 1 
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:55:53 01/28/2018 
// Design Name: 
// Module Name:    TLU_DacCtl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_DacCtl(
                        CLK,
                        NRST_X,
                        DATA_IN0,
                        DATA_IN1,
                        DATA_IN2,
                        DATA_IN3,
                        DAC_IN_START,
                        SCLK,
                        DIN,
                        FS,
                        BUSY
                    ) ;
                     
    input         CLK ;
    input         NRST_X ;
    input  [15:0] DATA_IN0 ;
    input  [15:0] DATA_IN1 ;
    input  [15:0] DATA_IN2 ;
    input  [15:0] DATA_IN3 ;
    input  [3:0]       DAC_IN_START ;

    
    output  [3:0]      SCLK ;
    output  [3:0]      DIN ;
    output  [3:0]      FS ;
    output  [3:0]      BUSY ;
    
    //
    parameter DELAY = `DELAY_UNIT ;
    parameter SCLK_WTH = 4'd1 ; // 2clock
    parameter SCLK_HWTH = SCLK_WTH >> 1 ; // 1clock
    parameter FS_END   = 6'd15 ;
    parameter DAC_END   = 6'd16 ;
   
    //
    reg    [3:0]  sclk_wth_cnt_r ;
    reg           dac_in_r ; 
    reg    [5:0]  sclk_cnt_r ;
    reg   [15:0]  data_in0_r ;
    reg   [15:0]  data_in1_r ;
    reg   [15:0]  data_in2_r ;
    reg   [15:0]  data_in3_r ;
    reg   [3:0]        dac_in_start_r ;
  
    wire          dac_end_trg ;
    wire          shift_trg ;
    
    wire   [3:0]       dac_in_start_pedge ;
        
    // DAC Start signal (positive edge)  
    always @( posedge CLK or negedge NRST_X ) begin
        if ( !NRST_X ) begin
            dac_in_start_r <= #DELAY 4'b0 ;
        end else begin
            dac_in_start_r <= #DELAY DAC_IN_START ;
	end
    end    
     
    assign #DELAY dac_in_start_pedge = DAC_IN_START & ~dac_in_start_r ;
    
    // DAC input state
    always @( posedge CLK or negedge NRST_X ) begin
        if ( !NRST_X ) 
            dac_in_r <= #DELAY 1'b0 ;
        else if ( dac_end_trg )
            dac_in_r <= #DELAY 1'b0 ;
        else if ( dac_in_start_pedge & 4'b1111 )
            dac_in_r <= #DELAY 1'b1 ;
    end    
    
    

    // SCLK generator
    always @( posedge CLK or negedge NRST_X ) begin
        if ( !NRST_X ) 
            sclk_wth_cnt_r <= #DELAY 4'd0 ;
        else if ( dac_in_r )  begin
            if ( shift_trg )
                sclk_wth_cnt_r <= #DELAY 4'd0 ;
            else
                sclk_wth_cnt_r <= #DELAY sclk_wth_cnt_r + 4'd1 ;
        end
        else 
            sclk_wth_cnt_r <= #DELAY 4'd0 ;            
    end    
   
    assign #DELAY shift_trg = ( sclk_wth_cnt_r == SCLK_WTH ) ;
    
    
    // SCLK counter
    always @( posedge CLK or negedge NRST_X ) begin
        if ( !NRST_X ) 
            sclk_cnt_r <= #DELAY 6'd0 ;
        else if ( dac_end_trg )
            sclk_cnt_r <= #DELAY 6'd0 ;
        else if ( shift_trg )
            sclk_cnt_r <= #DELAY sclk_cnt_r + 6'd1 ;
    end
 
//    assign #DELAY dac_end_trg   = ( sclk_cnt_r == FS_END ) & shift_trg ;
    assign #DELAY dac_end_trg   = ( sclk_cnt_r == DAC_END ) & shift_trg ;
          
      
    // Shift register
    always @( posedge CLK or negedge NRST_X ) begin
        if ( !NRST_X ) begin
            data_in0_r <= #DELAY 16'h0000 ;
            data_in1_r <= #DELAY 16'h0000 ;
            data_in2_r <= #DELAY 16'h0000 ;
            data_in3_r <= #DELAY 16'h0000 ;
        end else if ( ~dac_in_r ) begin
            data_in0_r <= #DELAY DATA_IN0 ;
            data_in1_r <= #DELAY DATA_IN1 ;
            data_in2_r <= #DELAY DATA_IN2 ;
            data_in3_r <= #DELAY DATA_IN3 ;
        end else if ( shift_trg ) begin
            data_in0_r <= #DELAY { data_in0_r[14:0], 1'b0 } ;
            data_in1_r <= #DELAY { data_in1_r[14:0], 1'b0 } ;
            data_in2_r <= #DELAY { data_in2_r[14:0], 1'b0 } ;
            data_in3_r <= #DELAY { data_in3_r[14:0], 1'b0 } ;
	end
    end
    
    // Output
    assign #DELAY SCLK[0] = ( sclk_wth_cnt_r <= SCLK_HWTH ) & dac_in_r ;
    assign #DELAY SCLK[1] = ( sclk_wth_cnt_r <= SCLK_HWTH ) & dac_in_r ;
    assign #DELAY SCLK[2] = ( sclk_wth_cnt_r <= SCLK_HWTH ) & dac_in_r ;
    assign #DELAY SCLK[3] = ( sclk_wth_cnt_r <= SCLK_HWTH ) & dac_in_r ;
    
    assign #DELAY BUSY = dac_in_r ;
    assign #DELAY FS[0]   = ~( dac_in_r & ( sclk_cnt_r <= FS_END ) );
    assign #DELAY FS[1]   = ~( dac_in_r & ( sclk_cnt_r <= FS_END ) );
    assign #DELAY FS[2]   = ~( dac_in_r & ( sclk_cnt_r <= FS_END ) );
    assign #DELAY FS[3]   = ~( dac_in_r & ( sclk_cnt_r <= FS_END ) );

    assign #DELAY DIN[0]  = data_in0_r[15] ;
    assign #DELAY DIN[1]  = data_in1_r[15] ;
    assign #DELAY DIN[2]  = data_in2_r[15] ;
    assign #DELAY DIN[3]  = data_in3_r[15] ;
   
endmodule // DAC_INPUT_TLV