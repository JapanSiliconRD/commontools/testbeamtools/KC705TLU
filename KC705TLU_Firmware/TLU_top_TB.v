`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:03:58 11/11/2015
// Design Name:   TLU_top
// Module Name:   /home/atlasj/work/Xilinx/Seabas/SeabasTLU/TLU_top_TB.v
// Project Name:  SeabasTLU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: TLU_top
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TLU_top_TB;

	// Inputs
	reg CLK50M;
	reg [3:0] NIMin;
	reg [7:0] SW;
	reg [3:0] BusyIn;
//	reg Dac_ConfigEn;
//	reg Dac_WriteEn;

	// Outputs
	wire [1:0] NIMout;
	wire [3:0] LED;
	wire [3:0] TrigOut;
	wire [1:0] Pin;

	// Instantiate the Unit Under Test (UUT)
	TLU_top uut (
		.CLK50M(CLK50M), 
		.NIMin(~NIMin), 
		.NIMout(NIMout), 
		.SW(SW), 
		.LED(LED)
//		.TrigOut(TrigOut), 
//		.BusyIn(BusyIn), 
//		.Pin(Pin)
	);

	initial begin
		// Initialize Inputs
		CLK50M = 1'b0;
		NIMin = 4'h0;
		SW = 8'h00;
		BusyIn = 4'h0;
		SW[4]=1'b1;
//		Dac_ConfigEn=1'b0;
//		Dac_WriteEn=1'b1;

      // Wait 100 ns for global reset to finish
      #100  NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #100 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #500 SW[0]=1'b1;
	   #0 SW[3]=1'b1;
      // pass through cosmic
      #100 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass stopped muon
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
		#0 SW[4]<=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // pass through cosmic
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass through cosmic
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass decay electron
      #5000 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // pass through cosmic
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass stopped muon
      #5000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // pass decay electron
      #4000 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // pass stopped muon
      #4000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      
      // pass through cosmic
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // pass through cosmic
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b1;
      #0 NIMin[3]=1'b1;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
      // decay electron upward
      #1000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass stopped muon
      #4000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      // pass stopped muon
      #1500000 NIMin[0]=1'b1;
      #0 NIMin[1]=1'b1;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      #100 NIMin[0]=1'b0;
      #0 NIMin[1]=1'b0;
      #0 NIMin[2]=1'b0;
      #0 NIMin[3]=1'b0;
      
		// Add stimulus here
            
  
   end
   parameter PERIOD=20;
   always begin
      CLK50M=1'b0;
      #(PERIOD/2) CLK50M=1'b1;
      #(PERIOD/2);
   end
        

endmodule

