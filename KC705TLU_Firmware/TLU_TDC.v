`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:51:31 11/18/2015 
// Design Name: 
// Module Name:    TLU_TDC 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_TDC(
    input CLK0,
    input RSTn,
    input start,
    input stop,
    input [32:0] MAXCOUNT,
    output [31:0] TDCcount, // TDCcount[31]==1'b1 indicate MAXCOUNT or 2^30 count
    output  GATEOUT
    );

   reg [1:0] 	  startbuf;
   reg [1:0] 	  stopbuf;
   reg [31:0] 	  syn_counter;
   reg 		  GATE;

   initial begin
      syn_counter <= 32'b0;
      GATE <=0;
      startbuf <= 2'b0;
      stopbuf <=2'b0;
   end
//   always @(posedge CLK0 or !RSTn) begin
   always @(posedge CLK0) begin
      if(!RSTn)begin
	 syn_counter <= 32'b0;
	 GATE <=0;
	 startbuf <= 2'b0;
	 stopbuf <=2'b0;
      end else begin
	 
	 startbuf <= {startbuf[0],start};
	 stopbuf <= {stopbuf[0],stop};
	 if(startbuf[0]==1'b1 & startbuf[1]==1'b0 & !GATE)begin
	    syn_counter <= 32'b0;
	    GATE<=1'b1;
	 end
	 if(stopbuf[0]==1'b1 & stopbuf[1]==1'b0 & GATE) begin
	    GATE<=1'b0;
	 end
	 if(GATE) begin
	    syn_counter <= syn_counter + 32'b1;
	 end
	 if((syn_counter==MAXCOUNT | syn_counter[30]==1'b1) & GATE) begin
	    syn_counter[31]<=1'b1; // overflow bit
	    GATE<=1'b0;
	 end
      end
   end
   assign TDCcount[31:0]=syn_counter[31:0]-32'b1;
   assign GATEOUT=GATE;
endmodule
