`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:29:18 11/19/2015 
// Design Name: 
// Module Name:    TLU_TrigEmulator 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////




module TLU_TrigEmulator(
    input CLK0, // 40MHz
    input RSTn,
    output [3:0] EMUROIin,
    output [3:0] EMUNIMin,
    output [3:0] EMUBusyIn
    );

//   parameter [32:0] CosmicInterval0 = 32'd39999996; // 1Hz
//   parameter [32:0] CosmicInterval1 = 32'd39999997; // 1Hz
//   parameter [32:0] CosmicInterval2 = 32'd39999999; // 1Hz
//   parameter [32:0] CosmicInterval3 = 32'd40000000; // 1Hz
   parameter [32:0] CosmicInterval0 = 32'd39996; // 1kHz
   parameter [32:0] CosmicInterval1 = 32'd39997; // 1kHz
   parameter [32:0] CosmicInterval2 = 32'd39999; // 1kHz
   parameter [32:0] CosmicInterval3 = 32'd40000; // 1kHz
//	  parameter [32:0] CosmicInterval0 = 32'd399996; // 100Hz
//   parameter [32:0] CosmicInterval1 = 32'd399997; // 100Hz
//   parameter [32:0] CosmicInterval2 = 32'd399999; // 100Hz
//   parameter [32:0] CosmicInterval3 = 32'd400000; // 100Hz
//	parameter [32:0] CosmicInterval0 = 32'd99996; // 200Hz
 //  parameter [32:0] CosmicInterval1 = 32'd99997; // 200Hz
//   parameter [32:0] CosmicInterval2 = 32'd99999; // 200Hz
//   parameter [32:0] CosmicInterval3 = 32'd100000; // 200Hz
//   parameter [32:0] CosmicInterval0 = 32'd3996; // 10kHz
//   parameter [32:0] CosmicInterval1 = 32'd3997; // 10kHz
//   parameter [32:0] CosmicInterval2 = 32'd3999; // 10kHz
//   parameter [32:0] CosmicInterval3 = 32'd4000; // 10kHz
//   parameter [32:0] CosmicInterval0 = 32'd396; // 100kHz
//   parameter [32:0] CosmicInterval1 = 32'd397; // 100kHz
//   parameter [32:0] CosmicInterval2 = 32'd399; // 100kHz
//   parameter [32:0] CosmicInterval3 = 32'd400; // 100kHz
//   parameter [32:0] CosmicInterval0 = 32'd796; // 50kHz
//   parameter [32:0] CosmicInterval1 = 32'd797; // 50kHz
//   parameter [32:0] CosmicInterval2 = 32'd799; // 50kHz
//   parameter [32:0] CosmicInterval3 = 32'd800; // 50kHz
   parameter [32:0] StoppedMuInterval0 = 32'd399999996; // 0.1Hz
   parameter [32:0] StoppedMuInterval1 = 32'd399999997; // 0.1Hz
   parameter [32:0] StoppedMuInterval2 = 32'd399999999; // 0.1Hz
   parameter [32:0] StoppedMuInterval3 = 32'd400000000; // 0.1Hz
   parameter [7:0] LIFETIME0 = 8'd86;    // 2.2us 
   parameter [7:0] LIFETIME1 = 8'd87;    // 2.2us 
   parameter [7:0] LIFETIME2 = 8'd89;    // 2.2us 
   parameter [7:0] LIFETIME3 = 8'd90;    // 2.2us 
   parameter [32:0] DELAYTIME = 32'd25000000; //delay 10ms


//   parameter [32:0] CosmicInterval0 = 32'd396; // 1Hz
//   parameter [32:0] CosmicInterval1 = 32'd397; // 1Hz
//   parameter [32:0] CosmicInterval2 = 32'd399; // 1Hz
//   parameter [32:0] CosmicInterval3 = 32'd400; // 1Hz
//   parameter [32:0] StoppedMuInterval0 = 32'd396; // 0.1Hz
//   parameter [32:0] StoppedMuInterval1 = 32'd397; // 0.1Hz
//   parameter [32:0] StoppedMuInterval2 = 32'd399; // 0.1Hz
//   parameter [32:0] StoppedMuInterval3 = 32'd400; // 0.1Hz
//   parameter [7:0] LIFETIME0 = 8'd86;    // 2.2us 
//   parameter [7:0] LIFETIME1 = 8'd87;    // 2.2us 
//   parameter [7:0] LIFETIME2 = 8'd89;    // 2.2us 
//   parameter [7:0] LIFETIME3 = 8'd90;    // 2.2us 
//   parameter [32:0] DELAYTIME = 32'd250; //delay 10ms

   
   reg [32:0] 	 firstdelay;
   reg [32:0] 	 countCosmic;
   reg [32:0] 	 countStoppedMu;
   reg [7:0] 	 countLife;
   reg [3:0] 	 emuroi;
   reg [3:0] 	 emunim;
   reg [3:0] 	 emubusy;
   reg 		 isup;
   
   assign EMUROIin = emuroi;
   assign EMUNIMin = emunim;
   assign EMUBusyIn = emubusy;
   
   initial begin
      emuroi<=4'b0;
      emunim<=4'b0;
      emubusy<=4'b0;
      countCosmic<=32'b0;
      countStoppedMu<=32'b0;
      countLife<=8'b1;
      firstdelay<=32'b1;
      isup<=1'b0;
   end
   //   always @(posedge CLK0 or !RSTn) begin
   always @(posedge CLK0) begin
      if(!RSTn)begin
	 emuroi<=4'b0;
	 emunim<=4'b0;
	 emubusy<=4'b0;
	 countCosmic<=32'b0;
	 countStoppedMu<=32'b0;
	 countLife<=8'b1;
	 firstdelay<=32'b1;
	 isup<=1'b0;
      end else begin
	 countCosmic<=countCosmic+32'b1;
	 if(firstdelay!=32'b0)begin
	    firstdelay<=firstdelay+32'b1;
	 end else begin
	    countStoppedMu<=countStoppedMu+32'b1;
	 end
	 if(firstdelay==DELAYTIME)begin
	    firstdelay<=32'b0;
	 end
	 if(countLife!=8'b0) begin
	    countLife<=countLife+8'b1;
	 end
	 // Generating Cosmic signal
	 if(countCosmic==CosmicInterval0)begin
	    emunim[0]<=1;
	    emunim[3]<=1;
	    emuroi<=1;
	 end else if(countCosmic==CosmicInterval1)begin
	    emunim[1]<=1;
	    emunim[2]<=1;
	 end else if(countCosmic==CosmicInterval2)begin
	    emunim[0]<=0;
	    emunim[3]<=0;
	 end else if(countCosmic==CosmicInterval3)begin
	    emunim[1]<=0;
	    emunim[2]<=0;
	    emuroi<=0;
	    countCosmic<=32'b0;
	 end
	 
	 // Decay Muon signal
	 if(countStoppedMu==StoppedMuInterval0)begin
	    emunim[0]<=1;
	    countLife<=8'b1;
	 end else if(countStoppedMu==StoppedMuInterval1)begin
	    emunim[1]<=1;
	 end else if(countStoppedMu==StoppedMuInterval2)begin
	    emunim[0]<=0;
	 end else if(countStoppedMu==StoppedMuInterval3)begin
	    emunim[1]<=0;
	 end
	 if(countLife==LIFETIME0)begin
	    if(isup==1'b0)emunim[2]<=1;
	    else emunim[0]<=1;
	 end else if(countLife==LIFETIME1)begin
	    if(isup==1'b0)emunim[3]<=1;
	    else emunim[1]<=1;
	 end else if(countLife==LIFETIME2)begin
	    if(isup==1'b0)emunim[2]<=0;
	    else emunim[0]<=0;
	 end else if(countLife==LIFETIME3)begin
	    if(isup==1'b0)emunim[3]<=0;
	    else emunim[1]<=0;
	    if(isup==1'b0) isup<=1'b1;
	    else  isup<=1'b0;
	    countLife<=8'b0;
	    countStoppedMu<=32'b0;
	 end
	 
	 
      end
   end
endmodule
