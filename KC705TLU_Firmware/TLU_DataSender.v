`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:07:58 11/15/2015 
// Design Name: 
// Module Name:    TLU_DataSender 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_DataSender(input CLK0,
		      input RSTn,
		      input [87:0] DataIn,
		      input DataValid,
		      output ReadValid,
		      output DataSenderReady,
		      output [7:0] ReadData
		      );

   parameter [3:0] din_counterMAX = 4'd11;

   reg [7:0] fifo0_wd;
   reg 	     fifo0_we;
   reg 	     fifo0_re;
   reg [3:0] din_counter;
   reg [7:0] datain0;
   reg [7:0] datain1;
   reg [7:0] datain2;
   reg [7:0] datain3;
   reg [7:0] datain4;
   reg [7:0] datain5;
   reg [7:0] datain6;
   reg [7:0] datain7;
   reg [7:0] datain8;
   reg [7:0] datain9;
   reg [7:0] datain10;
   reg 	     readval;
   reg 	     RST_FIFO;
   

   wire      fifo0_empty;
   wire [7:0]     fifo0_out;
   wire      fifo0_valid;
   

   initial begin
      fifo0_wd <=8'b0;
      fifo0_we <=1'b1;
      fifo0_re <=1'b0;
      din_counter<=4'b0;
      datain0<=8'b0;
      datain1<=8'b0;
      datain2<=8'b0;
      datain3<=8'b0;
      datain4<=8'b0;
      datain5<=8'b0;
      datain6<=8'b0;
      datain7<=8'b0;
      datain8<=8'b0;
      datain9<=8'b0;
      datain10<=8'b0;
      readval<=1'b0;
      RST_FIFO<=1'b0;
   end
   always @(posedge CLK0) begin
      if(!RSTn) begin
	 fifo0_wd <=8'b0;
	 fifo0_we <=1'b1;
	 fifo0_re <=1'b0;
	 din_counter<=4'b0;
	 datain0<=8'b0;
	 datain1<=8'b0;
	 datain2<=8'b0;
	 datain3<=8'b0;
	 datain4<=8'b0;
	 datain5<=8'b0;
	 datain6<=8'b0;
	 datain7<=8'b0;
	 datain8<=8'b0;
	 datain9<=8'b0;
	 datain10<=8'b0;
	 readval<=1'b0;
	 RST_FIFO<=1'b0;
      end else begin
	 if(din_counter != din_counterMAX) din_counter<=din_counter+4'b1;

	 if(DataValid)begin
	    datain0<=DataIn[87:80];
	    datain1<=DataIn[79:72];
	    datain2<=DataIn[71:64];
	    datain3<=DataIn[63:56];
	    datain4<=DataIn[55:48];
	    datain5<=DataIn[47:40];
	    datain6<=DataIn[39:32];
	    datain7<=DataIn[31:24];
	    datain8<=DataIn[23:16];
	    datain9<=DataIn[15:8]; 
	    datain10<=DataIn[7:0];  
	    din_counter<=4'b0;
	 end

	 if(din_counter==4'd0)begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain0;
	    readval<=1'b1;
	 end else if(din_counter==4'd1) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain1;
	    readval<=1'b1;
	 end else if(din_counter==4'd2) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain2;
	    readval<=1'b1;
	 end else if(din_counter==4'd3) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain3;
	    readval<=1'b1;
	 end else if(din_counter==4'd4) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain4;
	    readval<=1'b1;
	 end else if(din_counter==4'd5) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain5;
	    readval<=1'b1;
	 end else if(din_counter==4'd6) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain6;
	    readval<=1'b1;
	 end else if(din_counter==4'd7) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain7;
	    readval<=1'b1;
	 end else if(din_counter==4'd8) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain8;
	    readval<=1'b1;
	 end else if(din_counter==4'd9) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain9;
	    readval<=1'b1;
	 end else if(din_counter==4'd10) begin
	    fifo0_we<=1'b1;
	    fifo0_wd<=datain10;
	    readval<=1'b1;
	 end else begin
	    fifo0_we<=1'b0;
	    readval<=1'b0;
	 end
	   
	 
	 if(!fifo0_empty) fifo0_re <= 1'b1;
	 else fifo0_re <= 1'b0;
	 
	 
	 
      end
      
   end
     
   
//   FIFO fifo(//.rst(!RSTn),
//	     .rst(RST_FIFO),
//	     .wr_clk(CLK0),
//	     .rd_clk(CLK0),
//	     .din(fifo0_wd),
//	     .wr_en(fifo0_we),
//	     .rd_en(fifo0_re),
//	     .dout(fifo0_out),
//	     .almost_full(fifo0_afull),
//	     .empty(fifo0_empty)
//	     );

   fifo_generator_v13_0 fifo(
	  .clk			(CLK0),//in	:
	  .rst			(RST_FIFO),//in	:
	  .din			(fifo0_wd),//in	:
	  .wr_en		(fifo0_we),//in	:
	  .full			(),//out	:
          .almost_full          (fifo0_afull),//out :
	  .dout			(fifo0_out),//out	:
	  .valid		(fifo0_valid),//out	:active hi
	  .rd_en		(fifo0_re),//in	:
	  .empty		(),//out	:
	  .data_count	(fifo0_count)//out	:[11:0]
	);



   
   assign DataSenderReady=~fifo0_afull;
   assign ReadData=fifo0_out;
   assign ReadValid=readval;
//   assign DataSenderReady=1'b0;
//   assign ReadData=fifo0_wd;
//   assign ReadValid=readval;
   
endmodule
