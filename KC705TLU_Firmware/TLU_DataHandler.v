`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    21:52:11 11/18/2015 
// Design Name: 
// Module Name:    TLU_DataHandler 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_DataHandler(
		       input CLK0,
		       input RSTn,
		       input [15:0] TriggerID,
		       input [3:0] TrigBit,
		       input TrigBeam,
		       input TrigCosmic,
		       input TrigStoppedmu,
		       input TrigDecayEle,
		       input [23:0] TimeStamp,
		       input [23:0] TDCcount,
		       input GATEOUT,
		       output [87:0] DataOUT,     // header(4)  MPPC(1)  TrigID(2)  Separator(1)  TimeStamp(3)
		       output DataValid
//		       output [7:0] DataOUT0, // DH[8:0],TID[15:0],TDC[23:0]
//		       output [7:0] DataOUT1, // DH[8:0],TID[15:0],TDC[23:0]
//		       output [7:0] DataOUT2, // DH[8:0],TID[15:0],TDC[23:0]
//		       output [7:0] DataOUT3, // DH[8:0],TID[15:0],TDC[23:0]
//		       output [7:0] DataOUT4, // DH[8:0],TID[15:0],TDC[23:0]
//		       output [7:0] DataOUT5 // DH[8:0],TID[15:0],TDC[23:0]
		       );
   parameter DHDH        =32'hffffffff;
   parameter SEPARATOR   =8'b0;
   parameter DH_BEAM     =4'b0000;  // f
   parameter DH_COSMIC   =4'b1010;  // a
   parameter DH_MUDECAYST=4'b1011;  // b
   parameter DH_NOSTARTDN=4'b1100;  // c
   parameter DH_MUDECAYUP=4'b1101;  // d
   parameter DH_MUDECAYDN=4'b1110;  // e

   parameter DataReadDelay=4'd10;
   
   reg [7:0] 			   DH;
   reg [15:0] 			   TID;
   reg [23:0] 			   TS;
   reg [23:0] 			   TDC;

   reg [1:0] 			   TrigBeamBuf;
   reg [1:0] 			   TrigCosmicBuf;
   reg [1:0] 			   TrigStoppedmuBuf;
   reg [1:0] 			   TrigDecayEleBuf; 
   reg 				   dataval;
   reg [3:0] 			   datavalcnt;
   
   initial begin 
      DH<=8'b0;
      TID<=16'b0;
      TS<=24'b0;
      TDC<=24'b0;
      TrigBeamBuf<=2'b0;
      TrigCosmicBuf<=2'b0;
      TrigStoppedmuBuf<=2'b0;
      TrigDecayEleBuf<=2'b0;
      dataval<=1'b0;
      datavalcnt<=4'b0;
   end

   always @(posedge CLK0 or negedge RSTn) begin
//   always @(posedge CLK0) begin
      if(!RSTn)begin
	 DH<=8'b0;
//	 DH<=DH_COSMIC;
	 TID<=16'b0;
	 TS<=24'b0;
	 TDC<=24'b0;
	 TrigBeamBuf<=2'b0;
	 TrigCosmicBuf<=2'b0;
	 TrigStoppedmuBuf<=2'b0;
	 TrigDecayEleBuf<=2'b0; 
	 dataval<=1'b0;
	 datavalcnt<=4'b0;
      end else begin
	 TrigBeamBuf<={TrigBeamBuf[0],TrigBeam};
	 TrigCosmicBuf<={TrigCosmicBuf[0],TrigCosmic};
	 TrigStoppedmuBuf<={TrigStoppedmuBuf[0],TrigStoppedmu};
	 TrigDecayEleBuf<={TrigDecayEleBuf[0],TrigDecayEle}; 

	 if(datavalcnt!=4'b1111)datavalcnt<=datavalcnt+4'b1;

	 if(TrigBeamBuf[0]==1'b1 & TrigBeamBuf[1]==1'b0) begin
	    DH<={DH_BEAM,TrigBit};
	    TID<=TriggerID;
	    TS<=TimeStamp;
	    TDC<=24'b0;
	    datavalcnt<=4'b0;
	 end
	 
//	 if(TrigCosmicBuf[0]==1'b1 & TrigCosmicBuf[1]==1'b0) begin
//	    DH<={DH_COSMIC,TrigBit};
//	    TID<=TriggerID;
//	    TDC<=24'b0;
//	    datavalcnt<=4'b0;
//	 end else if(TrigStoppedmuBuf[0]==1'b1 &TrigStoppedmuBuf[1]==1'b0) begin
//	    if(!GATEOUT) begin
//	       DH<={DH_MUDECAYST,TrigBit};
//	       TID<=TriggerID;
//	       TDC<=24'b0;
//	       datavalcnt<=4'b0;
//	    end else begin
//	       DH<={DH_MUDECAYUP,TrigBit};
//	       TID<=TriggerID;
//	       TDC<=TDCcount[23:0];
//	       datavalcnt<=4'b0;
//	    end
//	 end else if(TrigDecayEleBuf[0]==1'b1 & TrigDecayEleBuf[1]==1'b0) begin 
//	    if(!GATEOUT) begin
//	       DH<={DH_NOSTARTDN,TrigBit};
//	       TID<=TriggerID;
//	       TDC<=24'b0;
//	       datavalcnt<=4'b0;
//	    end else begin
//	       DH<={DH_MUDECAYDN,TrigBit};
//	       TID<=TriggerID;
//	       TDC<=TDCcount[23:0];
//	       datavalcnt<=4'b0;
//	    end
//	 end
	 
//	 if(TrigStoppedmuBuf[0]==1'b1 &TrigStoppedmuBuf[1]==1'b0 & !GATEOUT) begin
//	    DH<=DH_MUDECAYST;
//	    TID<=TriggerID;
//	    TDC<=24'b0;
//	    datavalcnt<=4'b0;
//	 end else  if(TrigDecayEleBuf[0]==1'b1 & TrigDecayEleBuf[1]==1'b0 & !GATEOUT) begin
//	    DH<=DH_NOSTARTDN;
//	    TID<=TriggerID;
//	    TDC<=24'b0;
//	    datavalcnt<=4'b0;
//	 end
//	 
//	 if(TrigStoppedmuBuf[0]==1'b1 &TrigStoppedmuBuf[1]==1'b0 & GATEOUT) begin
//	    DH<=DH_MUDECAYUP;
//	    TID<=TriggerID;
//	    TDC<=TDCcount[23:0];
//	    datavalcnt<=4'b0;
//	 end else  if(TrigDecayEleBuf[0]==1'b1 & TrigDecayEleBuf[1]==1'b0 & GATEOUT) begin
//	    DH<=DH_MUDECAYDN;
//	    TID<=TriggerID;
//	    TDC<=TDCcount[23:0];
//	    datavalcnt<=4'b0;
//	 end
	 
	 if(datavalcnt==DataReadDelay)begin
	    dataval<=1'b1;
	 end else begin
	    dataval<=1'b0;
	 end
      end
   end

//   assign DataOUT={DH,TID,TDC};
   assign DataOUT={DHDH,DH,TID,SEPARATOR,TS};   // header(4)  MPPC(1)  TrigID(2)  Separator(1)  TimeStamp(3)
   assign DataValid=dataval;

//   assign DataOUT0 = DH;
//   assign DataOUT1 = TID[15:8];
//   assign DataOUT2 = TID[7:0];
//   assign DataOUT3 = TDC[23:16];   
//   assign DataOUT4 = TDC[15:8];   
//   assign DataOUT5 = TDC[7:0];   

endmodule
