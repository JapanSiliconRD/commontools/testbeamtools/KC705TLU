`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:37:17 11/15/2015 
// Design Name: 
// Module Name:    TLU_TrigMaker 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_TrigMaker(input CLK0,
		     input  RSTn,
		     input Debug,
		     input TrigReady,
		     input [3:0] dout,
		     input [2:0] BusyIn,
		     input   ROIin,
		     input  CLK100K,
		     input   clk_20M_s,
		     output  TrigBeam,
		     output  TrigCosmic,
		     output  TrigStoppedmu,
		     output  TrigDecayEle,
		     output [23:0] TimeStamp,
		     output  ROIBit,
		     output BusyOut,
		     output [3:0] TrigBit,
		     output [15:0] TriggerID,
		     output [15:0] TriggerCosmicID
		     );

//   parameter [7:0] TrigWidth=8'd20;
//   parameter [7:0] TrigWidth=8'd60;
//   parameter [7:0] TrigWidth=8'd80;
   parameter [7:0] TrigWidth=8'd60;
 //  parameter  [31:0] GBusyWidth = 32'd200; // * 50ns  = 10us 
//   parameter  [31:0] GBusyWidth = 32'd2000; // * 50ns  = 100us 
//   parameter  [31:0] GBusyWidth = 32'd4000; // * 50ns  = 200us 
//   parameter  [31:0] GBusyWidth = 32'd10000; // * 50ns  = 500us 
//   parameter  [31:0] GBusyWidth = 32'd20000; // * 50ns  =1ms
// parameter  [31:0] GBusyWidth = 32'd22000; // * 50ns  =1.1ms
parameter  [31:0] GBusyWidth = 32'd24000; // * 50ns  =1.2ms
//   parameter  [31:0] GBusyWidth = 32'd30000; // * 50ns  =1.5ms
//   parameter  [31:0] GBusyWidth = 32'd40000; // * 50ns  =2ms
//   parameter  [31:0] GBusyWidth = 32'd60000; // * 50ns  =3ms

	
   reg [1:0] 		     ROIBUF;
   reg [1:0] 		     TRIGBUF0; // trig buffer for NIM0
   reg [1:0] 		     TRIGBUF1; // trig buffer for NIM1
   reg [1:0] 		     TRIGBUF2; // trig buffer for NIM2
   reg [1:0] 		     TRIGBUF3; // trig buffer for NIM3
   reg  		     ROIFIRED;
   reg [3:0] 		     TRIGGERED;
   reg [7:0] 		     ROIKEEPCNT;
   reg [7:0] 		     TRIGKEEPCNT0;
   reg [7:0] 		     TRIGKEEPCNT1;
   reg [7:0] 		     TRIGKEEPCNT2;
   reg [7:0] 		     TRIGKEEPCNT3;

   reg [15:0] 		     trigid;
   reg [15:0] 		     trigcosmicid;
   reg 			     trigbeam;
   reg  		     trigcos;
   reg  		     trigsmu;
   reg  		     trigmud;
   reg  		     TRIGBEAMr;
   reg  		     TRIGCOSMICr;
   reg  		     TRIGSTOPPEDMUr;
   reg  		     TRIGMUDECAY;
   reg  [1:0]		     TRIGBEAMBUF;
   reg  [1:0]		     TRIGCOSMICBUF;
   reg  [1:0]		     TRIGSTOPPEDMUBUF;
   reg  [1:0]		     TRIGMUDECAYBUF;
   reg  [7:0]		     TRIGBEAMCNT;
   reg  [7:0]		     TRIGCOSMICCNT;
   reg  [7:0]		     TRIGSTOPPEDMUCNT;
   reg  [7:0]		     TRIGMUDECAYCNT;
   reg [23:0] 		     TIMESTAMPr;
   reg 			     GlobalBusy;   //gbusy
   reg [32:0] 		     gbusycnt;    //gbusy 
   reg [2:0] 		     TRIGBEAMFORGBUSY; // gbusy   
   
   initial begin
      GlobalBusy<=1'b0;
      gbusycnt<=GBusyWidth;
      ROIBUF<=2'b0;
      TRIGBUF0<=2'b0;
      TRIGBUF1<=2'b0;
      TRIGBUF2<=2'b0;
      TRIGBUF3<=2'b0;
      ROIFIRED<=4'b0;
      TRIGGERED<=4'b0;
      ROIKEEPCNT<=8'b0;
      TRIGKEEPCNT0<=8'b0;
      TRIGKEEPCNT1<=8'b0;
      TRIGKEEPCNT2<=8'b0;
      TRIGKEEPCNT3<=8'b0;
      trigbeam<=1'b0;
      trigcos<=1'b0;
      trigsmu<=1'b0;
      trigmud<=1'b0;
      TRIGBEAMr<=1'b0;
      TRIGCOSMICr<=1'b0;
      TRIGSTOPPEDMUr<=1'b0;
      TRIGMUDECAY <= 1'b0;
      TRIGBEAMBUF<=2'b0;
      TRIGCOSMICBUF<=2'b0;
      TRIGSTOPPEDMUBUF<=2'b0;
      TRIGMUDECAYBUF <= 2'b0;
      TRIGBEAMCNT<=8'b0;
      TRIGCOSMICCNT<=8'b0;
      TRIGSTOPPEDMUCNT<=8'b0;
      TRIGMUDECAYCNT<=8'b0;
      trigid<=16'b0;
      trigcosmicid<=16'b0;
      TIMESTAMPr<=24'b0;
      TRIGBEAMFORGBUSY<=2'b0;
   end
   always @(posedge CLK100K) begin
      if(!RSTn)begin
	 TIMESTAMPr<=24'b0;
      end else begin
	 if(TIMESTAMPr!=24'hffffff) begin
	    TIMESTAMPr <= TIMESTAMPr + 24'b1;
	 end else begin
	    TIMESTAMPr <=24'b0;
	 end
      end
   end

   always @(posedge clk_20M_s) begin // gbusy
      TRIGBEAMFORGBUSY<={TRIGBEAMFORGBUSY[0],trigbeam};
      if(TRIGBEAMFORGBUSY[1]==1'b0 && TRIGBEAMFORGBUSY[0]==1'b1)begin
         gbusycnt<=32'b0;
         GlobalBusy<=1;
      end else begin
         if(gbusycnt!=GBusyWidth) begin
            gbusycnt <= gbusycnt + 32'b1;
         end else begin
            gbusycnt <= 32'b0;
            GlobalBusy<=0;
         end
      end
   end //
   
   always @(posedge CLK0) begin
      if(!RSTn)begin
	 ROIBUF<=2'b0;
	 TRIGBUF0<=2'b0;
	 TRIGBUF1<=2'b0;
	 TRIGBUF2<=2'b0;
	 TRIGBUF3<=2'b0;
	 ROIFIRED<=1'b0;
	 TRIGGERED<=4'b0;
	 ROIKEEPCNT<=8'b0;
	 TRIGKEEPCNT0<=8'b0;
	 TRIGKEEPCNT1<=8'b0;
	 TRIGKEEPCNT2<=8'b0;
	 TRIGKEEPCNT3<=8'b0;
	 trigbeam<=1'b0;
	 trigcos<=1'b0;
	 trigsmu<=1'b0;
	 trigmud<=1'b0;
	 TRIGBEAMr<=1'b0;
	 TRIGCOSMICr<=1'b0;
	 TRIGSTOPPEDMUr<=1'b0;
	 TRIGMUDECAY <= 1'b0;
	 TRIGBEAMBUF<=2'b0;
	 TRIGCOSMICBUF<=2'b0;
	 TRIGSTOPPEDMUBUF<=2'b0;
	 TRIGMUDECAYBUF <= 2'b0;
	 TRIGBEAMCNT<=8'b0;
	 TRIGCOSMICCNT<=8'b0;
	 TRIGSTOPPEDMUCNT<=8'b0;
	 TRIGMUDECAYCNT<=8'b0;
	 trigid<=16'b0;
	 trigcosmicid<=16'b0;
      end else begin
	 if(!TrigReady) begin
	    ROIBUF<=2'b0;
	    TRIGBUF0<=2'b0;
	    TRIGBUF1<=2'b0;
	    TRIGBUF2<=2'b0;
	    TRIGBUF3<=2'b0;
	    ROIFIRED<=1'b0;
	    TRIGGERED<=4'b0;
	    ROIKEEPCNT<=8'b0;
	    TRIGKEEPCNT0<=8'b0;
	    TRIGKEEPCNT1<=8'b0;
	    TRIGKEEPCNT2<=8'b0;
	    TRIGKEEPCNT3<=8'b0;
	    trigbeam<=1'b0;
	    trigcos<=1'b0;
	    trigsmu<=1'b0;
	    trigmud<=1'b0;
	    TRIGBEAMr<=1'b0;
	    TRIGCOSMICr<=1'b0;
	    TRIGSTOPPEDMUr<=1'b0;
	    TRIGMUDECAY <= 1'b0;
	    TRIGBEAMBUF<=2'b0;
	    TRIGCOSMICBUF<=2'b0;
	    TRIGSTOPPEDMUBUF<=2'b0;
	    TRIGMUDECAYBUF <= 2'b0;
	    TRIGBEAMCNT<=8'b0;
	    TRIGCOSMICCNT<=8'b0;
	    TRIGSTOPPEDMUCNT<=8'b0;
	    TRIGMUDECAYCNT<=8'b0;
	    trigid<=16'b0;
	    trigcosmicid<=16'b0;
	 end else begin
	    ROIBUF<={ROIBUF[0],ROIin};
	    TRIGBUF0<={TRIGBUF0[0],dout[0]};
	    TRIGBUF1<={TRIGBUF1[0],dout[1]};
	    TRIGBUF2<={TRIGBUF2[0],dout[2]};
	    TRIGBUF3<={TRIGBUF3[0],dout[3]};
	    
	    
//	    if(~(BusyIn[0] | BusyIn[1] | BusyIn[2] | BusyIn[3])) begin
//	    if(~(BusyIn[0] | BusyIn[1] | BusyIn[2])) begin
//       if(~(BusyIn[0] | BusyIn[1] | BusyIn[2]| GlobalBusy )) begin
 //            if(1'b1) begin

	    if(~BusyIn[0]) begin
	       if(ROIBUF[0]==1'b1 & ROIBUF[1]==1'b0) begin
		  ROIFIRED <= 1'b1;
	       end
	       if(TRIGBUF0[0]==1'b1 & TRIGBUF0[1]==1'b0) begin
		  TRIGGERED[0] <= 1'b1;
	       end
	       if(TRIGBUF1[0]==1'b1 & TRIGBUF1[1]==1'b0) begin
		  TRIGGERED[1] <= 1'b1;
	       end
	       if(TRIGBUF2[0]==1'b1 & TRIGBUF2[1]==1'b0) begin
		  TRIGGERED[2] <= 1'b1;
	       end
	       if(TRIGBUF3[0]==1'b1 & TRIGBUF3[1]==1'b0) begin
		  TRIGGERED[3] <= 1'b1;
	       end
	    end
	    
	    if(ROIFIRED) begin
	       if(ROIKEEPCNT!=TrigWidth) 
		 ROIKEEPCNT<=ROIKEEPCNT + 8'b1;
	       else begin
		  ROIKEEPCNT<=8'b0000;
		  ROIFIRED <= 1'b0;
	       end
	    end
	    if(TRIGGERED[0]) begin
	       if(TRIGKEEPCNT0!=TrigWidth) 
		 TRIGKEEPCNT0<=TRIGKEEPCNT0 + 8'b1;
	       else begin
		  TRIGKEEPCNT0<=8'b0000;
		  TRIGGERED[0] <= 1'b0;
	       end
	    end
	    if(TRIGGERED[1]) begin
	       if(TRIGKEEPCNT1!=TrigWidth) 
		 TRIGKEEPCNT1<=TRIGKEEPCNT1 + 8'b1;
	       else begin
		  TRIGKEEPCNT1<=8'b0000;
		  TRIGGERED[1] <= 1'b0;
	       end
	    end
	    
	    if(TRIGGERED[2]) begin
	       if(TRIGKEEPCNT2!=TrigWidth) 
		 TRIGKEEPCNT2<=TRIGKEEPCNT2 + 8'b1;
	       else begin
		  TRIGKEEPCNT2<=8'b0000;
		  TRIGGERED[2] <= 1'b0;
	       end
	    end
	    if(TRIGGERED[3]) begin
	       if(TRIGKEEPCNT3!=TrigWidth) 
		 TRIGKEEPCNT3<=TRIGKEEPCNT3 + 8'b1;
	       else begin
		  TRIGKEEPCNT3<=8'b0000;
		  TRIGGERED[3] <= 1'b0;
	       end
	    end
	    
	    
      // 1 MPPC Scintilator Triggers
    if((TRIGGERED[0]==1'b1 & Debug) |
	       (TRIGGERED[0]==1'b1 & !Debug)) begin
//    if(1)begin
//	    if((TRIGGERED[0]==1'b1 & ROIFIRED & Debug) |
//		    (TRIGGERED[0]==1'b1 & ROIFIRED & !Debug)) begin // 1MPPC and ROI test
		    

      // 2 MPPC Scintilator Triggers
//	    if(((TRIGGERED[0]==1'b1 | TRIGGERED[1]==1'b1) & Debug) |
//	       ((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1) & !Debug)) begin
		 	 	 
	    // 2 MPPC Scintilator + ROI  Triggers
//	    if(((TRIGGERED[0]==1'b1 | TRIGGERED[1]==1'b1 | 
//		 ROIFIRED) & Debug) |
//	       ((TRIGGERED[0]==1'b1 & TRIGGERED[0]==1'b1  &
//		 ROIFIRED) & !Debug)) begin

//		if(ROIFIRED) begin

//  4 MPPC Scintilator with ROI 	    
// 	    if(((TRIGGERED[0]==1'b1 | TRIGGERED[1]==1'b1 |
//		 TRIGGERED[2]==1'b1 | TRIGGERED[3]==1'b1 | 
//		 ROIFIRED) & Debug) |
//	       ((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1 &
//		 TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1 &
//		 ROIFIRED) & !Debug)) begin
		 
// 4 MPPC Scintilator without ROI
//	    if(((TRIGGERED[0]==1'b1 | TRIGGERED[1]==1'b1 |
//		 TRIGGERED[2]==1'b1 | TRIGGERED[3]==1'b1 ) & Debug) |
//	       ((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1 &
//		 TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1 ) & !Debug)) begin



	       trigbeam<= 1'b1;
	    end
	    else begin
	       trigbeam<= 1'b0;
	    end
	    
	    if(((TRIGGERED[0]==1'b1 | TRIGGERED[1]==1'b1 |
		 TRIGGERED[2]==1'b1 | TRIGGERED[3]==1'b1) & Debug) |
	       ((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1 &
		 TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1) & !Debug)) begin
	       trigcos<= 1'b1;
	    end
	    else begin
	       trigcos<= 1'b0;
	    end
	    
	    if((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1) &
	       ~(TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1)) begin
	       trigsmu<=1'b1;
	    end 
	    else begin
	       trigsmu<=1'b0;
	    end
	    
	    if(((TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1) &
		~(TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1)) |
	       (~(TRIGGERED[0]==1'b1 & TRIGGERED[1]==1'b1) &
		(TRIGGERED[2]==1'b1 & TRIGGERED[3]==1'b1))) begin
	       trigmud<=1'b1;
	    end 
	    else begin
	       trigmud<=1'b0;
	    end
	    TRIGBEAMBUF<={TRIGBEAMBUF[0],trigbeam};
	    TRIGCOSMICBUF<={TRIGCOSMICBUF[0],trigcos};
	    TRIGSTOPPEDMUBUF<={TRIGSTOPPEDMUBUF[0],trigsmu};
	    TRIGMUDECAYBUF <= {TRIGMUDECAYBUF[0],trigmud};
	    


	    if(~TRIGCOSMICr & ~TRIGSTOPPEDMUr & ~TRIGMUDECAY) begin
	       if(TRIGCOSMICBUF[0]==1'b1 & TRIGCOSMICBUF[1]==1'b0)begin
		  TRIGCOSMICr<=1'b1;
		  trigcosmicid <= trigcosmicid + 16'b1;
	       end
	       if(TRIGSTOPPEDMUBUF[0]==1'b1 & TRIGSTOPPEDMUBUF[1]==1'b0)begin
		  TRIGSTOPPEDMUr<=1'b1;
		  trigcosmicid <= trigcosmicid + 16'b1;
	       end
	       if(TRIGMUDECAYBUF[0]==1'b1 & TRIGMUDECAYBUF[1]==1'b0)begin
		  TRIGMUDECAY<=1'b1;
		  trigcosmicid <= trigcosmicid + 16'b1;
	       end
	    end
	    
	    if(TRIGBEAMBUF[0]==1'b1 & TRIGBEAMBUF[1]==1'b0)begin
	       TRIGBEAMr<=1'b1;
	       trigid <= trigid + 16'b1;
	    end
	    
	    if(TRIGBEAMr) begin
	       if(TRIGBEAMCNT!=TrigWidth) 
		 TRIGBEAMCNT<=TRIGBEAMCNT + 8'b1;
	       else begin
		  TRIGBEAMCNT<=8'b0000;
		  TRIGBEAMr <= 1'b0;
	       end
	    end

	    if(TRIGCOSMICr) begin
	       if(TRIGCOSMICCNT!=TrigWidth) 
		 TRIGCOSMICCNT<=TRIGCOSMICCNT + 8'b1;
	       else begin
		  TRIGCOSMICCNT<=8'b0000;
		  TRIGCOSMICr <= 1'b0;
	       end
	    end
	    if(TRIGSTOPPEDMUr) begin
	       if(TRIGSTOPPEDMUCNT!=TrigWidth) 
		 TRIGSTOPPEDMUCNT<=TRIGSTOPPEDMUCNT + 8'b1;
	       else begin
		  TRIGSTOPPEDMUCNT<=8'b0000;
		  TRIGSTOPPEDMUr <= 1'b0;
	       end
	    end
	    if(TRIGMUDECAY) begin
	       if(TRIGMUDECAYCNT!=TrigWidth) 
		 TRIGMUDECAYCNT<=TRIGMUDECAYCNT + 8'b1;
	       else begin
		  TRIGMUDECAYCNT<=8'b0000;
		  TRIGMUDECAY <= 1'b0;
	       end
	    end
	    

	 end // else: !if(!SW0)
      end // 
   end
   
   assign TrigBeam=TRIGBEAMr;
   assign TrigCosmic=TRIGCOSMICr;
   assign TrigStoppedmu=TRIGSTOPPEDMUr;
   assign TrigDecayEle=TRIGMUDECAY;
   assign TriggerID=trigid;
   assign TriggerCosmicID=trigcosmicid;
   assign ROIBit=ROIFIRED;
   assign TrigBit={TRIGGERED[0],TRIGGERED[1],TRIGGERED[2],TRIGGERED[3]};
   assign TimeStamp=TIMESTAMPr;
   assign BusyOut=GlobalBusy;

   
//   assign Pin[0]=TrigReady;
//   assign Pin[1]=TRIGCOSMICr;
//   assign Pin[2]=trigcos;
//   assign Pin[3]=trigid[0];
//   assign Pin[0]=TRIGBUF0[0];
//   assign Pin[1]=TRIGBUF1[1];
//   assign Pin[2]=TRIGGERED[0];
//   assign Pin[3]=BusyIn[0];
   
endmodule
