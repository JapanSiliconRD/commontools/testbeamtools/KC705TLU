set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSout[8]}]

set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVCMOSin[6]}]

set_property IOSTANDARD LVCMOS25 [get_ports {NIMin[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMin[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMin[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMin[3]}]

set_property IOSTANDARD LVCMOS25 [get_ports {NIMout[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMout[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMout[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {NIMout[3]}]
	     
set_property IOSTANDARD LVCMOS15 [get_ports {SW3}]
set_property IOSTANDARD LVCMOS15 [get_ports {SW4}]
set_property IOSTANDARD LVCMOS15 [get_ports {SW6}]

set_property IOSTANDARD LVCMOS15 [get_ports {LED[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {LED[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED[7]}]

set_property IOSTANDARD LVCMOS25 [get_ports {LVDSin[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVDSin[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVDSout[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LVDSout[1]}]

set_property IOSTANDARD LVCMOS25 [get_ports {dout[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {dout[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {dout[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {dout[3]}]
	     
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_LDAC[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_LDAC[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_LDAC[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_LDAC[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports {DAC_PRE[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_PRE[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_PRE[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_PRE[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports {DAC_FS[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_FS[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_FS[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_FS[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports {DAC_DIN[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_DIN[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_DIN[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_DIN[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports {DAC_SCLK[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_SCLK[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_SCLK[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {DAC_SCLK[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports {XADC_GPIO[0]}]	     
set_property IOSTANDARD LVCMOS25 [get_ports {XADC_GPIO[1]}]	     
set_property IOSTANDARD LVCMOS25 [get_ports {XADC_GPIO[2]}]	     
set_property IOSTANDARD LVCMOS25 [get_ports {XADC_GPIO[3]}]	     

set_property IOSTANDARD LVCMOS25 [get_ports GMII_COL]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_CRS]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_GTXCLK]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_MDC]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_MDIO]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_RSTn]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_RXD[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_RX_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_RX_DV]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_RX_ER]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {GMII_TXD[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_TX_CLK]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_TX_EN]
set_property IOSTANDARD LVCMOS25 [get_ports GMII_TX_ER]
set_property IOSTANDARD LVCMOS25 [get_ports I2C_SDA]
set_property IOSTANDARD LVCMOS25 [get_ports I2C_SCL]
set_property IOSTANDARD LVCMOS25 [get_ports GPIO_SWITCH_0]
set_property IOSTANDARD LVCMOS15 [get_ports SW_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_200MP_IN]
set_property IOSTANDARD LVDS [get_ports SYSCLK_200MN_IN]

	     


# LPC_LA06_P
set_property PACKAGE_PIN AK20 [get_ports {LVCMOSout[0]}] 
# LPC_LA06_N
set_property PACKAGE_PIN AK21 [get_ports {LVCMOSout[1]}] 
# LPC_LA10_P
set_property PACKAGE_PIN AJ24 [get_ports {LVCMOSout[2]}] 
# LPC_LA10_N
set_property PACKAGE_PIN AK25 [get_ports {LVCMOSout[3]}] 
# LPC_LA09_P
set_property PACKAGE_PIN AK23 [get_ports {LVCMOSout[4]}] 
# LPC_LA09_N
set_property PACKAGE_PIN AK24 [get_ports {LVCMOSout[5]}] 
# LPC_LA13_P
set_property PACKAGE_PIN AB24 [get_ports {LVCMOSout[6]}] 
# LPC_LA13_N
set_property PACKAGE_PIN AC25 [get_ports {LVCMOSout[7]}] 
# LPC_LA14_P
set_property PACKAGE_PIN AD21 [get_ports {LVCMOSout[8]}] 

# LPC_LA23_P
set_property PACKAGE_PIN AH26 [get_ports {LVCMOSin[0]}] 
# LPC_LA23_N
set_property PACKAGE_PIN AH27 [get_ports {LVCMOSin[1]}] 
# LPC_LA26_P
set_property PACKAGE_PIN AK29 [get_ports {LVCMOSin[2]}] 
# LPC_LA26_N
set_property PACKAGE_PIN AK30 [get_ports {LVCMOSin[3]}] 
# LPC_LA27_N
set_property PACKAGE_PIN AJ29 [get_ports {LVCMOSin[4]}] 
# LPC_LA27_P
set_property PACKAGE_PIN AJ28 [get_ports {LVCMOSin[5]}] 
# LPC_LA14_N
set_property PACKAGE_PIN AE21 [get_ports {LVCMOSin[6]}] 

# LPC_LA15_N
set_property PACKAGE_PIN AD24 [get_ports {NIMin[0]}] 
# LPC_LA16_N			     
set_property PACKAGE_PIN AD22 [get_ports {NIMin[1]}] 
# LPC_LA15_P			     
set_property PACKAGE_PIN AC24 [get_ports {NIMin[2]}] 
# LPC_LA16_P			     
set_property PACKAGE_PIN AC22 [get_ports {NIMin[3]}] 

# LPC_LA11_N
set_property PACKAGE_PIN AF25 [get_ports {NIMout[0]}] 
# LPC_LA12_N			     	
set_property PACKAGE_PIN AB20 [get_ports {NIMout[1]}] 
# LPC_LA11_P			     	
set_property PACKAGE_PIN AE25 [get_ports {NIMout[2]}] 
# LPC_LA12_P			     	
set_property PACKAGE_PIN AA20 [get_ports {NIMout[3]}] 

# LPC_LA08_P
set_property PACKAGE_PIN AJ22 [get_ports {LVDSin[0]}] 
# LPC_LA08_N				
set_property PACKAGE_PIN AJ23 [get_ports {LVDSin[1]}] 
# LPC_LA07_P				
set_property PACKAGE_PIN AG25 [get_ports {LVDSout[0]}] 
# LPC_LA07_N				
set_property PACKAGE_PIN AJ21 [get_ports {LVDSout[1]}] 

# LPC_LA32_N
set_property PACKAGE_PIN AA30 [get_ports {dout[0]}] 
# LPC_LA31_N
set_property PACKAGE_PIN AE29 [get_ports {dout[1]}] 
# LPC_LA24_N
set_property PACKAGE_PIN AH30 [get_ports {dout[2]}] 
# LPC_LA21_P
set_property PACKAGE_PIN AG27 [get_ports {dout[3]}] 

# LPC_LA32_P
set_property PACKAGE_PIN Y30 [get_ports {DAC_LDAC[0]}] 
# LPC_LA31_P				 
set_property PACKAGE_PIN AD29 [get_ports {DAC_LDAC[1]}] 
# LPC_LA24_P			
set_property PACKAGE_PIN AG30 [get_ports {DAC_LDAC[2]}] 
# LPC_LA22_P			
set_property PACKAGE_PIN AJ27 [get_ports {DAC_LDAC[3]}] 

# LPC_LA33_N
set_property PACKAGE_PIN AC30 [get_ports {DAC_PRE[0]}] 
# LPC_LA28_N			
set_property PACKAGE_PIN AF30 [get_ports {DAC_PRE[1]}] 
# LPC_LA25_N			
set_property PACKAGE_PIN AD26 [get_ports {DAC_PRE[2]}] 
# LPC_LA19_N			
set_property PACKAGE_PIN AK26 [get_ports {DAC_PRE[3]}] 

# LPC_LA33_P
set_property PACKAGE_PIN AC29 [get_ports {DAC_FS[0]}] 
# LPC_LA28_P				 
set_property PACKAGE_PIN AE30 [get_ports {DAC_FS[1]}] 
# LPC_LA25_P				 
set_property PACKAGE_PIN AC26 [get_ports {DAC_FS[2]}] 
# LPC_LA20_N				 
set_property PACKAGE_PIN AF27 [get_ports {DAC_FS[3]}] 

# LPC_LA30_N
set_property PACKAGE_PIN AB30 [get_ports {DAC_DIN[0]}] 
# LPC_LA29_N				 
set_property PACKAGE_PIN AF28 [get_ports {DAC_DIN[1]}] 
# LPC_LA21_N			 
set_property PACKAGE_PIN AG28 [get_ports {DAC_DIN[2]}] 
# LPC_LA19_P				 
set_property PACKAGE_PIN AJ26 [get_ports {DAC_DIN[3]}] 

# LPC_LA30_P
set_property PACKAGE_PIN AB29 [get_ports {DAC_SCLK[0]}] 
# LPC_LA29_P				 
set_property PACKAGE_PIN AE28 [get_ports {DAC_SCLK[1]}] 
# LPC_LA22_N		
set_property PACKAGE_PIN AK28 [get_ports {DAC_SCLK[2]}] 
# LPC_LA20_P		
set_property PACKAGE_PIN AF26 [get_ports {DAC_SCLK[3]}] 


set_property PACKAGE_PIN AB25 [get_ports {XADC_GPIO[0]}] 
set_property PACKAGE_PIN AA25 [get_ports {XADC_GPIO[1]}] 
set_property PACKAGE_PIN AB28 [get_ports {XADC_GPIO[2]}] 
set_property PACKAGE_PIN AA27 [get_ports {XADC_GPIO[3]}] 

###########################################################
#
# Test for HPC connector
#
###########################################################
#### HPC_LA06_P
###set_property PACKAGE_PIN H30 [get_ports {LVCMOSout[0]}] 
#### HPC_LA06_N
###set_property PACKAGE_PIN G30 [get_ports {LVCMOSout[1]}] 
#### HPC_LA10_P
###set_property PACKAGE_PIN D29 [get_ports {LVCMOSout[2]}] 
#### HPC_LA10_N
###set_property PACKAGE_PIN C30 [get_ports {LVCMOSout[3]}] 
#### HPC_LA09_P
###set_property PACKAGE_PIN B30 [get_ports {LVCMOSout[4]}] 
#### HPC_LA09_N
###set_property PACKAGE_PIN A30 [get_ports {LVCMOSout[5]}] 
#### HPC_LA13_P
###set_property PACKAGE_PIN A25 [get_ports {LVCMOSout[6]}] 
#### HPC_LA13_N
###set_property PACKAGE_PIN A26 [get_ports {LVCMOSout[7]}] 
#### HPC_LA14_P
###set_property PACKAGE_PIN B28 [get_ports {LVCMOSout[8]}] 
###
#### HPC_LA23_P
###set_property PACKAGE_PIN B22 [get_ports {LVCMOSin[0]}] 
#### HPC_LA23_N
###set_property PACKAGE_PIN A22 [get_ports {LVCMOSin[1]}] 
#### HPC_LA26_P
###set_property PACKAGE_PIN B18 [get_ports {LVCMOSin[2]}] 
#### HPC_LA26_N
###set_property PACKAGE_PIN A18 [get_ports {LVCMOSin[3]}] 
#### HPC_LA27_N
###set_property PACKAGE_PIN B19 [get_ports {LVCMOSin[4]}] 
#### HPC_LA27_P
###set_property PACKAGE_PIN C19 [get_ports {LVCMOSin[5]}] 
#### HPC_LA14_N
###set_property PACKAGE_PIN A28 [get_ports {LVCMOSin[6]}] 




set_property PACKAGE_PIN AG5 [get_ports {SW3}] 
set_property PACKAGE_PIN AB12 [get_ports {SW4}] 
set_property PACKAGE_PIN AC6 [get_ports {SW6}] 

set_property PACKAGE_PIN AB8 [get_ports {LED[0]}] 
set_property PACKAGE_PIN AA8 [get_ports {LED[1]}] 
set_property PACKAGE_PIN AC9 [get_ports {LED[2]}] 
set_property PACKAGE_PIN AB9 [get_ports {LED[3]}] 
set_property PACKAGE_PIN AE26 [get_ports {LED[4]}] 
set_property PACKAGE_PIN G19 [get_ports {LED[5]}] 
set_property PACKAGE_PIN E18 [get_ports {LED[6]}] 
set_property PACKAGE_PIN F16 [get_ports {LED[7]}] 

#set_property PACKAGE_PIN D21 [get_ports {HPCLED[0]}] 
#set_property PACKAGE_PIN C21 [get_ports {HPCLED[1]}] 
#set_property PACKAGE_PIN H21 [get_ports {HPCLED[2]}] 
#set_property PACKAGE_PIN H22 [get_ports {HPCLED[3]}] 

set_property PACKAGE_PIN AD11 [get_ports SYSCLK_200MN_IN]
set_property PACKAGE_PIN AD12 [get_ports SYSCLK_200MP_IN]
set_property PACKAGE_PIN L20 [get_ports GMII_RSTn]
set_property PACKAGE_PIN M27 [get_ports GMII_TX_EN]
set_property PACKAGE_PIN N27 [get_ports {GMII_TXD[0]}]
set_property PACKAGE_PIN N25 [get_ports {GMII_TXD[1]}]
set_property PACKAGE_PIN M29 [get_ports {GMII_TXD[2]}]
set_property PACKAGE_PIN L28 [get_ports {GMII_TXD[3]}]
set_property PACKAGE_PIN J26 [get_ports {GMII_TXD[4]}]
set_property PACKAGE_PIN K26 [get_ports {GMII_TXD[5]}]
set_property PACKAGE_PIN L30 [get_ports {GMII_TXD[6]}]
set_property PACKAGE_PIN J28 [get_ports {GMII_TXD[7]}]
set_property PACKAGE_PIN N29 [get_ports GMII_TX_ER]
set_property PACKAGE_PIN U27 [get_ports GMII_RX_CLK]
set_property PACKAGE_PIN R28 [get_ports GMII_RX_DV]
set_property PACKAGE_PIN U30 [get_ports {GMII_RXD[0]}]
set_property PACKAGE_PIN U25 [get_ports {GMII_RXD[1]}]
set_property PACKAGE_PIN T25 [get_ports {GMII_RXD[2]}]
set_property PACKAGE_PIN U28 [get_ports {GMII_RXD[3]}]
set_property PACKAGE_PIN R19 [get_ports {GMII_RXD[4]}]
set_property PACKAGE_PIN T27 [get_ports {GMII_RXD[5]}]
set_property PACKAGE_PIN T26 [get_ports {GMII_RXD[6]}]
set_property PACKAGE_PIN T28 [get_ports {GMII_RXD[7]}]
set_property PACKAGE_PIN V26 [get_ports GMII_RX_ER]
set_property PACKAGE_PIN R30 [get_ports GMII_CRS]
set_property PACKAGE_PIN W19 [get_ports GMII_COL]
set_property PACKAGE_PIN J21 [get_ports GMII_MDIO]
set_property PACKAGE_PIN K30 [get_ports GMII_GTXCLK]
set_property PACKAGE_PIN M28 [get_ports GMII_TX_CLK]
set_property PACKAGE_PIN R23 [get_ports GMII_MDC]
set_property PACKAGE_PIN L21 [get_ports I2C_SDA]
set_property PACKAGE_PIN K21 [get_ports I2C_SCL]
set_property PACKAGE_PIN Y29 [get_ports GPIO_SWITCH_0]
set_property PACKAGE_PIN AA12 [get_ports SW_N]

create_clock -period 5.000 -name SYSCLK_200MP_IN -waveform {0.000 2.500} [get_ports SYSCLK_200MP_IN]
create_clock -period 40.000 -name GMII_TX_CLK -waveform {0.000 20.000} [get_ports GMII_TX_CLK]
create_clock -period 8.000 -name GMII_RX_CLK -waveform {0.000 4.000} [get_ports GMII_RX_CLK]
set_clock_groups -group [get_clocks GMII_TX_CLK] -group [get_clocks CLK_125M] -logically_exclusive

set_false_path -from [get_pins CNT_RST_reg/C] -to [get_pins RX_CNT_reg*/CLR]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.CONFIGRATE 6 [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]


set_max_delay -datapath_only -from [get_pins {RX_CNT_reg[6]/C}] -to [get_pins GMII_1000M_reg/D] 5.000

set_false_path -from [get_pins GMII_1000M_reg/C]
set_false_path -through [get_nets RST_EEPROM]

set_property IOB false [get_cells -hierarchical -filter {name =~ */GMII_RXCNT/IOB_RD_*}]
set_property IOB false [get_cells -hierarchical -filter {name =~ */GMII_RXCNT/IOB_RDV}]
set_property IOB false [get_cells -hierarchical -filter {name =~ */GMII_RXCNT/IOB_RERR}]

set_input_delay -clock [get_clocks GMII_RX_CLK] -min 0.5 [get_port GMII_RXD*]
set_input_delay -clock [get_clocks GMII_RX_CLK] -min 0.5 [get_port GMII_RX_ER]
set_input_delay -clock [get_clocks GMII_RX_CLK] -min 0.5 [get_port GMII_RX_DV]
set_input_delay -clock [get_clocks GMII_RX_CLK] -max 5.5 [get_port GMII_RXD*]
set_input_delay -clock [get_clocks GMII_RX_CLK] -max 5.5 [get_port GMII_RX_ER]
set_input_delay -clock [get_clocks GMII_RX_CLK] -max 5.5 [get_port GMII_RX_DV]

set_max_delay -from [get_port GMII_MDIO] 10
set_min_delay -from [get_port GMII_MDIO] 0
set_max_delay -from [get_port I2C_SDA] 10
set_min_delay -from [get_port I2C_SDA] 0

set_property IOB true [get_cells -hierarchical -filter {name =~ */GMII_TXCNT/IOB_TD_*}]
set_property IOB true [get_cells -hierarchical -filter {name =~ */GMII_TXCNT/IOB_TEN}]

set_max_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TXD*] 30
set_min_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TXD*] 0
set_max_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TX_EN] 30
set_min_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TX_EN] 0
set_max_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TX_ER] 30
set_min_delay -from [get_clocks GMII_TX_CLK] -to [get_port GMII_TX_ER] 0


set_max_delay -datapath_only -from [get_clocks CLK_125M] -to [get_port GMII_GTXCLK] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_125M] -to [get_port GMII_TX_EN] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_125M] -to [get_port GMII_TXD*] 3.2
set_max_delay -datapath_only -from [get_clocks CLK_125M] -to [get_port GMII_TX_ER] 3.2

set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port GMII_MDC] 10
set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port GMII_MDIO] 10
set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port GMII_RSTn] 10
set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port I2C_SCL] 10
set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port I2C_SDA] 10

set_max_delay -from [get_port LVCMOSin*] 10
set_min_delay -from [get_port LVCMOSin*] 0
set_max_delay -datapath_only -from [get_clocks SYSCLK_200MP_IN] -to [get_port LVCMOSout*] 10
