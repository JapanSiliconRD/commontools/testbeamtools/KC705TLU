`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:04:30 11/11/2015 
// Design Name: 
// Module Name:    TLU_top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_top(input CLK50M,
	       input [7:0] NIMin, 
	       output [5:0] NIMout,
	       input [7:0] SW,
	       output [7:0] LED,
//	       input  ROIin,
	       input [6:0] LVCMOSin,
//			 input [6:0] CMOSin, //revB cmosin
	       output [8:0] LVCMOSout,
//			 output [8:0] CMOSout, //revB cmosout
			 input [1:0] LVDSin,
			 output [1:0] LVDSout,
	       // for SiTCP
	       // User I/F
	       output USR_CLK,           // out : Clock
	       input  USR_ACTIVE,        // in  : TCP established
	       input  USR_CLOSE_REQ,     // in  : Request to close a TCP Connection
	       output USR_CLOSE_ACK,     // out : Acknowledge for USR_CLOSE_REQ
	       // TCP Tx
	       input  USR_TX_AFULL,      // in  : Almost full flag of a TCP Tx FIFO
	       output USR_TX_WE,         // out : TCP Tx Data write enable
	       output [7:0] USR_TX_WD,    // out : TCP Tx Data[7:0]
	       // TCP Rx
	       input  USR_RX_EMPTY,      // in  : Empty flag of a TCP RX flag
	       output USR_RX_RE,	 // out : TCP Rx Data Read enable
	       input  USR_RX_RV,	 // in  : TCP Rx data valid
	       input  [7:0] USR_RX_RD,	 // in  : TCP Rx data[7:0]
	       // Register Access
	       input  REG_CLK,           // in : Clock
	       input  REG_ACT,		 // in	: Access Active
	       output REG_DI,		 // out: Serial Data Output
	       input  REG_DO,		 // in	: Serial Data Input
	       input  [3:0] dout,
			 //output dout2_p,
			 //output dout2_n,
			 //output dout3_p,
			 //output dout3_n,
	       input  [3:0] dout_cmos,
	       output [3:0] DAC_LDAC,
	       output [3:0] DAC_PRE,
	       output [3:0] DAC_FS,
	       output [3:0] DAC_DIN,
	       output [3:0] DAC_SCLK,
	       input [11:0] GPIO
	       );

   
   reg 		            RSTn_RUN;
   reg 			    RST_DCMPLL;
   reg 			    RSTn_TDC;
   reg 			    RSTn_DataHandler;
   reg 			    RSTn_TrigMaker;
   reg 			    RSTn_Emulator;
   reg 			    RSTn_DataSendler;
   reg 			    TrigReady;
   reg 			    orUsrCloseAck;
   reg 			    orUsrTxWe;
   reg [7:0] 		    orUsrTxWd;
   reg 			    irUsrActive;
   reg 			    irUsrCloseReq;
   reg 			    irUsrTxAfull;
   reg 			    irUsrRxEmpty;
   reg 			    irUsrRxRv;
   reg [7:0] 		    irUsrRxRd;

   
   reg [87:0] 		    dataoutcopy;
   reg 			    dataoutseri;
   reg [7:0] 		    dataoutcnt;
			    
			    
   reg [7:0] 		    commandincopy;
   reg 			    commandinseri;
   reg [3:0]		    commandincnt;
	   
   reg [4:0] 		    FIFODelayCNT;
   reg [3:0] 		    DatawidthCNT;

   reg [3:0] 		    REG_DAC_ADD;
   reg [3:0] 		    REG_DAC_VAL1;
   reg [3:0] 		    REG_DAC_VAL2;
   reg [3:0] 		    REG_DAC_VAL3;
   
   reg 			    Dac_ConfigEn;
   reg 			    Dac_WriteEn;
   reg [3:0] 		    Dac_Add;
   reg [11:0] 		    Dac_Data;


   wire [3:0] 		    dac_ldac_s;
   wire [3:0] 		    dac_pre_s;
   wire [3:0] 		    dac_fs_s;
   wire [3:0] 		    dac_din_s;
   wire [3:0] 		    dac_sclk_s;
   

   
   reg [6:0] 		    debugled;
   
   wire 		    CLK100K;
   wire [15:0] 		    TriggerCosmicID;
   wire [15:0] 		    TriggerID;
   wire [31:0] 		    TDCcount;
   wire [87:0] 		    DataOUT;
   
   wire [3:0] 		    EMUROIin;
   wire [3:0] 		    EMUNIMin;
   wire [3:0] 		    EMUBusyIn;
   wire 		    DataValid;
   wire [7:0] 		    ReadData;
   wire [23:0]		    TimeStamp;
   wire 		    TrigBeam;
   wire 		    TrigCosmic;
   wire 		    TrigStoppedmu;
   wire 		    TrigDecayEle;
   wire 		    ROIBit;
   wire [3:0]		    TrigBit;
   wire 		    TrigCosmicLEDOUT;
   wire 		    TrigStoppedmuLEDOUT;
   wire 		    TrigDecayEleLEDOUT;

   // wire for SIO_REG
   reg [7:0] 		    regaddr      ;
   wire [7:0] 		    ADDR00    ;
   wire [7:0] 		    ADDR01    ;
   wire [7:0] 		    ADDR02    ;
   wire [7:0] 		    ADDR03    ;
   wire [7:0] 		    ADDR04    ;
   wire [7:0] 		    ADDR05    ;
   wire [7:0] 		    ADDR06    ;
   wire [7:0] 		    ADDR07    ;
   wire [7:0] 		    ADDR08    ;
   wire [7:0] 		    ADDR09    ;
   wire [7:0] 		    ADDR0A    ;
   wire [7:0] 		    ADDR0B    ;
   wire [7:0] 		    ADDR0C    ;
   wire [7:0] 		    ADDR0D    ;
   wire [7:0] 		    ADDR0E    ;
   wire [7:0] 		    ADDR0F    ;
   wire [7:0] 		    ADDR10    ;
   wire [7:0] 		    ADDR11    ;
   wire [7:0] 		    ADDR12    ;
   wire [7:0] 		    ADDR13    ;
   wire [7:0] 		    ADDR14    ;
   wire [7:0] 		    ADDR15    ;
   wire [7:0] 		    ADDR16    ;
   wire [7:0] 		    ADDR17    ;
   wire [7:0] 		    ADDR18    ;
   wire [7:0] 		    ADDR19    ;
   wire [7:0] 		    ADDR1A    ;
   wire [7:0] 		    ADDR1B    ;
   wire [7:0] 		    ADDR1C    ;
   wire [7:0] 		    ADDR1D    ;
   wire [7:0] 		    ADDR1E    ;
   wire [7:0] 		    ADDR1F    ;
//   reg [7:0] 		    ADDR [0:31];
   reg [27:0] 		    counter;

   
   initial begin
      RST_DCMPLL<=1'b0;
      RSTn_TDC<=1'b1;
      RSTn_DataHandler<=1'b1;
      RSTn_Emulator<=1'b1;
      RSTn_TrigMaker<=1'b1;
//      RSTn_TrigMaker<=SW[0];
      RSTn_DataSendler<=1'b1;
      RSTn_RUN<=1'b0;
      TrigReady<=1'b1;
      orUsrCloseAck<=1'b0;
      orUsrTxWe<=1'b0;
      orUsrTxWd<=8'b0;
      irUsrActive<=1'b0;
      irUsrCloseReq<=1'b0;
      irUsrTxAfull<=1'b0;
      dataoutcopy<=88'b0;
      dataoutseri<=1'b0;
      dataoutcnt<=7'b0;
      commandincopy<=8'b0;
      commandinseri<=1'b0;
      commandincnt<=4'b0;
      Dac_ConfigEn<=1'b0;
      Dac_WriteEn<=1'b0;
      Dac_Add<=4'b0;
      Dac_Data<=12'd2370;

      REG_DAC_ADD<=4'b0;
      REG_DAC_VAL1<=4'b0;
      REG_DAC_VAL2<=4'b0;
      REG_DAC_VAL3<=4'b0;
      debugled<=7'b0;
     
   end
   
   DCMPLLGen DCMPLLGen(.CLKIN_IN(CLK50M),
		       .RST_IN(RST_DCMPLL),
		       .CLKOUT0_OUT(clk_40M_s),
		       .CLKOUT1_OUT(clk_20M_s),
		       .CLKOUT2_OUT(clk_100M_s),
		       .CLKOUT3_OUT(clk_160M_s),
		       .LOCKED_OUT(dcmpll_locked_s)
		       );

   TLU_TrigEmulator emu(.CLK0(clk_40M_s),
//			.RSTn(RSTn_Emulator),
			.RSTn(!SW[0]&RSTn_RUN),
//			.RSTn(!SW[0]),
			.EMUROIin(EMUROIin),
			.EMUNIMin(EMUNIMin),
			.EMUBusyIn(EMUBusyIn)
			);
   
   
   TLU_TrigMaker tlutm(.CLK0(clk_100M_s),
//		       .RSTn(RSTn_TrigMaker),
		       .RSTn(!SW[0]&RSTn_RUN),
//		       .RSTn(!SW[0]),
		       .Debug(~SW[1]),
                       .TrigReady(TrigReady),
///////////////////////////////////////////////////////////////
//    For Emulation 
		       .NIMin(EMUNIMin),  // Emulated nim signal
		       .BusyIn(EMUBusyIn),  // Emulated busy signal
		       .ROIin(EMUROIin),  // Emulated ROI signal
///////////////////////////////////////////////////////////////////
//    For Test
//		       .NIMin({NIMin[4],NIMin[4],NIMin[4],NIMin[4]}), 
//		       .BusyIn({LVCMOSin[0],LVCMOSin[0],LVCMOSin[0]}),
//		       .ROIin(~NIMin[0]),
//		       .ROIin(NIMin[4]),
///////////////////////////////////////////////////////////////////
//    For Actual Trigger
//		       .NIMin(~NIMin), 
//		       .BusyIn({NIMin[4],LVCMOSin[0],LVCMOSin[1]}),

//		       .ROIin(~LVCMOSin[2]),
		       
		       .CLK100K(CLK100K),
		       .TrigBeam(TrigBeam),
		       .TimeStamp(TimeStamp),
		       .TrigCosmic(TrigCosmic),
		       .TrigStoppedmu(TrigStoppedmu),
		       .TrigDecayEle(TrigDecayEle),
		       .ROIBit(ROIBit),
		       .TrigBit(TrigBit),
		       .TriggerID(TriggerID),
		       .TriggerCosmicID(TriggerCosmicID)
		       );
   
   
   TLU_TDC tlutdc(.CLK0(clk_100M_s),
//		  .RSTn(RSTn_TDC),
		  .RSTn(!SW[0]&RSTn_RUN),
//		  .RSTn(!SW[0]),
		  .start(TrigStoppedmu),
		  .stop(TrigDecayEle),
		  .MAXCOUNT(32'd100000), // 100k x 10us = 1ms 
		  .TDCcount(TDCcount),
		  .GATEOUT(GATEOUT)
		  );

   TLU_DataHandler tludh(.CLK0(clk_100M_s),
//			 .RSTn(RSTn_DataHandler),
			 .RSTn(!SW[0]&RSTn_RUN),
//			 .RSTn(!SW[0]),
			 .TriggerID(TriggerID),
			 .TrigBit(TrigBit),
			 .TimeStamp(TimeStamp),
			 .TrigBeam(TrigBeam),
			 .TrigCosmic(TrigCosmic),
			 .TrigStoppedmu(TrigStoppedmu),
			 .TrigDecayEle(TrigDecayEle),
			 .TDCcount(TDCcount[23:0]),
			 .GATEOUT(GATEOUT),
			 .DataOUT(DataOUT),
			 .DataValid(DataValid)
			 );
   
   TLU_DataSender tluds(.CLK0(clk_100M_s),
//			.RSTn(RSTn_DataSendler),
			.RSTn(!SW[0]&RSTn_RUN),
//			.RSTn(!SW[0]),
			.DataIn(DataOUT),
			.DataValid(DataValid),
			.ReadValid(ReadValid),
			.DataSenderReady(DataSenderReady),
			.ReadData(ReadData)
			);
   
   
   TLU_ShowLED tluled(.CLK0(clk_100M_s),
		      .TrigCosmic(TrigCosmic),
		      .TrigStoppedmu(TrigStoppedmu),
		      .TrigDecayEle(TrigDecayEle),
		      .TrigCosmicLEDOUT(TrigCosmicLEDOUT),
		      .TrigStoppedmuLEDOUT(TrigStoppedmuLEDOUT),
		      .TrigDecayEleLEDOUT(TrigDecayEleLEDOUT)
		      );
				
    wire  [15:0] DATA_IN0 ;
    wire  [15:0] DATA_IN1 ;
    wire  [15:0] DATA_IN2 ;
    wire  [15:0] DATA_IN3 ;
    wire  [3:0]  DAC_ADDR0 ;
    wire  [3:0]  DAC_ADDR1 ;
    wire  [3:0]  DAC_ADDR2 ;
    wire  [3:0]  DAC_ADDR3 ;
    wire  [11:0] DAC_DATA0 ;
    wire  [11:0] DAC_DATA1 ;
    wire  [11:0] DAC_DATA2 ;
    wire  [11:0] DAC_DATA3 ;
    wire  [3:0]       DAC_IN_START ;
    wire  [3:0]       BUSY ;

   wire [3:0] 	 PRE ;
   wire [3:0] 	 LDAC ;
   wire [3:0] 	 SCLK ;
   wire [3:0] 	 DIN ;
   wire [3:0] 	 FS ;
   
//    assign  DAC_IN_START   = ADDR[0][0] ;
    assign  DAC_IN_START[0]   = ADDR00[0] ;
    assign  DAC_IN_START[1]   = ADDR0A[0] ;
    assign  DAC_IN_START[2]   = ADDR10[0] ;
    assign  DAC_IN_START[3]   = ADDR1A[0] ;

    assign  DAC_ADDR0[3:0]  = ADDR01[3:0] ;
    assign  DAC_ADDR1[3:0]  = ADDR0B[3:0] ;
    assign  DAC_ADDR2[3:0]  = ADDR11[3:0] ;
    assign  DAC_ADDR3[3:0]  = ADDR1B[3:0] ;
   
    assign  DAC_DATA0[7:0]  = ADDR02[7:0] ;
    assign  DAC_DATA1[7:0]  = ADDR0C[7:0] ;
    assign  DAC_DATA2[7:0]  = ADDR12[7:0] ;
    assign  DAC_DATA3[7:0]  = ADDR1C[7:0] ;

    assign  DAC_DATA0[11:8] = ADDR03[3:0] ;
    assign  DAC_DATA1[11:8] = ADDR0D[3:0] ;
    assign  DAC_DATA2[11:8] = ADDR13[3:0] ;
    assign  DAC_DATA3[11:8] = ADDR1D[3:0] ;

    assign  DATA_IN0        = { DAC_ADDR0[3:0], DAC_DATA0[11:0] } ;
    assign  DATA_IN1        = { DAC_ADDR1[3:0], DAC_DATA1[11:0] } ;
    assign  DATA_IN2        = { DAC_ADDR2[3:0], DAC_DATA2[11:0] } ;
    assign  DATA_IN3        = { DAC_ADDR3[3:0], DAC_DATA3[11:0] } ;
    
    assign  PRE[0]            = ADDR04[0] ;
    assign  PRE[1]            = ADDR0E[0] ;
    assign  PRE[2]            = ADDR14[0] ;
    assign  PRE[3]            = ADDR1E[0] ;

    assign  LDAC[0]           = ADDR05[0] ;
    assign  LDAC[1]           = ADDR0F[0] ;
    assign  LDAC[2]           = ADDR15[0] ;
    assign  LDAC[3]           = ADDR1F[0] ;


   
    TLU_DacCtl tludac (
                        .CLK(clk_20M_s),
                        .NRST_X(!SW[0]),
                        .DATA_IN0(DATA_IN0),
                        .DATA_IN1(DATA_IN1),
                        .DATA_IN2(DATA_IN2),
                        .DATA_IN3(DATA_IN3),
                        .DAC_IN_START(DAC_IN_START),
                        .SCLK(SCLK),
                        .DIN(DIN),
                        .FS(FS),
                        .BUSY(BUSY)
    ) ;


   assign   DAC_LDAC[0]=~LDAC[0];
   assign   DAC_LDAC[1]=~LDAC[1];
   assign   DAC_LDAC[2]=~LDAC[2];
   assign   DAC_LDAC[3]=~LDAC[3];
   assign	DAC_PRE[0]=~PRE[0];
   assign	DAC_PRE[1]=~PRE[1];
   assign	DAC_PRE[2]=~PRE[2];
   assign	DAC_PRE[3]=~PRE[3];
   assign	DAC_FS[0]=FS[0];
   assign	DAC_FS[1]=FS[1];
   assign	DAC_FS[2]=FS[2];
   assign	DAC_FS[3]=FS[3];
   assign	DAC_DIN[0]=DIN[0];
   assign	DAC_DIN[1]=DIN[1];
   assign	DAC_DIN[2]=DIN[2];
   assign	DAC_DIN[3]=DIN[3];
   assign	DAC_SCLK[0]=SCLK[0];
   assign	DAC_SCLK[1]=SCLK[1];
   assign	DAC_SCLK[2]=SCLK[2];
   assign	DAC_SCLK[3]=SCLK[3];

//   TLU_DacCtl tludac(.CLK0(clk_20M_s),
//		     .Dac_ConfigEn(Dac_ConfigEn),
//		     .Dac_WriteEn(Dac_WriteEn),
////		     .Dac_ConfigEn(SW[3]),
////		     .Dac_WriteEn(SW[4]),
//		     .Dac_Add(Dac_Add),
//		     .Dac_Data(Dac_Data),
//		     .DAC_LDAC(dac_ldac_s),
//		     .DAC_PRE(dac_pre_s),
//		     .DAC_FS(dac_fs_s),
//		     .DAC_DIN(dac_din_s),
//		     .DAC_SCLK(dac_sclk_s)
//		     );
   
   wire [31:0] SIO_ADDR; 
   wire [7:0]  SIO_WD;
   wire        SIO_WE;
   wire        SIO_RE;
   wire        SIO_ACK;
   wire        SIO_RV;
   wire [7:0]  SIO_RD;
   
   //--- RBCP works only when a TCP connection is established.
   //--- (USR_ACTIVE is used as the RST signal.)
   SIO_SLAVE sioslave(
		      .RSTn     (!SW[0]), // in : System reset
		      .FILL_ADDR(32'h0), // in : Filled address for narow address-width
		      // Serial I/F
		      .SCK(REG_CLK), // in : Clock
		      .SCS(REG_ACT), // in : Active
		      .SI (REG_DI ), // out: Data input
		      .SO (REG_DO ), // in : Data output
		      // Register I/F
		      .REG_ADDR(SIO_ADDR),    // out : Address[31:0]
		      .REG_WD  (SIO_WD[7:0]), // out : Data[7:0]
		      .REG_WE  (SIO_WE     ), // out : Write enable
		      .REG_RE  (SIO_RE     ), // out : Read enable
		      .REG_ACK (SIO_ACK    ), // in  : Access acknowledge
		      .REG_RV  (SIO_RV     ), // in  : Read valid
		      .REG_RD  (SIO_RD[7:0])  // in  : Read data[7:0]
		      );

    SIO_REG SIO_REG(
        .SCK(REG_CLK),  // in  : Clock
        .RSTn(!SW[0]),    // in  : System reset
    
        .ADDR00(ADDR00),
        .ADDR01(ADDR01),
        .ADDR02(ADDR02),
        .ADDR03(ADDR03),
        .ADDR04(ADDR04),
        .ADDR05(ADDR05),
        .ADDR06(ADDR06),
        .ADDR07(ADDR07),
        .ADDR08(ADDR08),
        .ADDR09(ADDR09),
        .ADDR0A(ADDR0A),
        .ADDR0B(ADDR0B),
        .ADDR0C(ADDR0C),
        .ADDR0D(ADDR0D),
        .ADDR0E(ADDR0E),
        .ADDR0F(ADDR0F),
        .ADDR10(ADDR10),
        .ADDR11(ADDR11),
        .ADDR12(ADDR12),
        .ADDR13(ADDR13),
        .ADDR14(ADDR14),
        .ADDR15(ADDR15),
        .ADDR16(ADDR16),
        .ADDR17(ADDR17),
        .ADDR18(ADDR18),
        .ADDR19(ADDR19),
        .ADDR1A(ADDR1A),
        .ADDR1B(ADDR1B),
        .ADDR1C(ADDR1C),
        .ADDR1D(ADDR1D),
        .ADDR1E(ADDR1E),
        .ADDR1F(ADDR1F),
        
        // Register I/F  
        .REG_ADDR(SIO_ADDR[31:0]),  // in  : Address[31:0]
        .REG_WD(SIO_WD[7:0]),       // in  : Data[7:0]
        .REG_WE(SIO_WE),            // in  : Write enable
        .REG_RE(SIO_RE),            // in  : Read enable
        .REG_ACK(SIO_ACK),          // out: Access acknowledge
        .REG_RV(SIO_RV),            // out: Read valid
        .REG_RD(SIO_RD[7:0])        // out: Read data[7:0]
    );

  // Register assign
//    always @( posedge clk_20M_s or negedge SW[0] ) begin
//        if ( !SW[0] ) begin
//            ADDR[0] <= 8'h00 ;
//            ADDR[1] <= 8'h00 ;
//            ADDR[2] <= 8'h00 ;
//            ADDR[3] <= 8'h00 ;
//            ADDR[4] <= 8'h00 ;
//            ADDR[5] <= 8'h00 ;
//            ADDR[6] <= 8'h00 ;
//            ADDR[7] <= 8'h00 ;
//            ADDR[8] <= 8'h00 ;
//            ADDR[9] <= 8'h00 ;
//            ADDR[10] <= 8'h00 ;
//            ADDR[11] <= 8'h00 ;
//            ADDR[12] <= 8'h00 ;
//            ADDR[13] <= 8'h00 ;
//            ADDR[14] <= 8'h00 ;
//            ADDR[15] <= 8'h00 ;
//            ADDR[16] <= 8'h00 ;
//            ADDR[17] <= 8'h00 ;
//            ADDR[18] <= 8'h00 ;
//            ADDR[19] <= 8'h00 ;
//            ADDR[20] <= 8'h00 ;
//            ADDR[21] <= 8'h00 ;
//            ADDR[22] <= 8'h00 ;
//            ADDR[23] <= 8'h00 ;
//            ADDR[24] <= 8'h00 ;
//            ADDR[25] <= 8'h00 ;
//            ADDR[26] <= 8'h00 ;
//            ADDR[27] <= 8'h00 ;
//            ADDR[28] <= 8'h00 ;
//            ADDR[29] <= 8'h00 ;
//            ADDR[30] <= 8'h00 ;
//            ADDR[31] <= 8'h00 ;
//        end
//        else begin
//            ADDR[0] <= ADDR00 ;
//            ADDR[1] <= ADDR01 ;
//            ADDR[2] <= ADDR02 ;
//            ADDR[3] <= ADDR03 ;
//            ADDR[4] <= ADDR04 ;
//            ADDR[5] <= ADDR05 ;
//            ADDR[6] <= ADDR06 ;
//            ADDR[7] <= ADDR07 ;
//            ADDR[8] <= ADDR08 ;
//            ADDR[9] <= ADDR09 ;
//            ADDR[10] <= ADDR0A ;
//            ADDR[11] <= ADDR0B ;
//            ADDR[12] <= ADDR0C ;
//            ADDR[13] <= ADDR0D ;
//            ADDR[14] <= ADDR0E ;
//            ADDR[15] <= ADDR0F ;
//            ADDR[16] <= ADDR10 ;
//            ADDR[17] <= ADDR11 ;
//            ADDR[18] <= ADDR12 ;
//            ADDR[19] <= ADDR13 ;
//            ADDR[20] <= ADDR14 ;
//            ADDR[21] <= ADDR15 ;
//            ADDR[22] <= ADDR16 ;
//            ADDR[23] <= ADDR17 ;
//            ADDR[24] <= ADDR18 ;
//            ADDR[25] <= ADDR19 ;
//            ADDR[26] <= ADDR1A ;
//            ADDR[27] <= ADDR1B ;
//            ADDR[28] <= ADDR1C ;
//            ADDR[29] <= ADDR1D ;
//            ADDR[30] <= ADDR1E ;
//            ADDR[31] <= ADDR1F ;
//        end
//    end
   
   
//   assign TrigOut[0]=TrigCosmic | TrigStoppedmu | TrigDecayEle;
//   assign TrigOut[1]=TrigCosmic | TrigStoppedmu | TrigDecayEle;
//   assign TrigOut[2]=TrigCosmic | TrigStoppedmu | TrigDecayEle;

   
//   assign LED[0] = TrigCosmicLEDOUT;
////   assign LED[1] = TrigStoppedmuLEDOUT;
////   assign LED[2] = TrigDecayEleLEDOUT;
////   assign LED[3] = GATEOUT;
//   assign LED[1] = debugled[4];
//   assign LED[2] = debugled[5];
//   assign LED[3] = debugled[6];
//   
//
//   assign LED[4] = debugled[0];
//   assign LED[5] = debugled[1];
//   assign LED[6] = debugled[2];
//   assign LED[7] = debugled[3];
   assign LED[7:0] = ADDR00[7:0];
//   assign LED[7:0] = { DAC_ADDR[3:0], DAC_DATA[3:0] };
   
   

   //assign LVCMOSout[0] = ~NIMin[0]; // HSIO2 trigger //for MPPC external& FE65
//   assign LVCMOSout[0] = CLK100K; // HSIO2 trigger
   //assign LVCMOSout[0] = EMUNIMin;
   assign LVCMOSout[0] = TrigBeam;
   assign LVCMOSout[1] = LVCMOSin[0];  // SPEC trigger
//   assign LVCMOSout[2] = NIMin[4];  // PicoScope A  :  SVX busy
//   assign LVCMOSout[2] = ~(NIMin[4]|LVCMOSin[0]|LVCMOSin[1]); // PicoScope A : Busy
   //   assign LVCMOSout[3] = LVCMOSin[1];  // PicoScope B  :  SPEC busy
//   assign LVCMOSout[3] = ROIBit;  // PicoScope B  :
//   assign LVCMOSout[4] = LVCMOSin[0];  // PicoScope C  :  HSIO2 busy
//   assign LVCMOSout[4] = ~LVCMOSin[3];  // PicoScope C  :  ROI tirgger
//   assign LVCMOSout[4] = CLK100K;  // PicoScope C  :  TimiStamp
//   assign LVCMOSout[4] = dataoutseri;  // PicoScope C  :  Dataout
//   assign LVCMOSout[4] = commandinseri;  // PicoScope C  :  Dataout
	assign LVCMOSout[2] = LVCMOSin[1];
	assign LVCMOSout[3] = LVCMOSin[2];
	assign LVCMOSout[4] = LVCMOSin[3];
	assign LVCMOSout[5] = LVCMOSin[4];
	assign LVCMOSout[6] = LVCMOSin[5];
	assign LVCMOSout[7] = LVCMOSin[6];
	assign LVCMOSout[8] = TrigBeam;

   assign LVDSout[0] = TrigBeam; // to SVX4
   assign LVDSout[1] = TrigBeam;

	//assign dout2[0] = TrigBeam;
	//assign dout2[1] = TrigBeam;
	//assign dout3[0] = TrigBeam;
	//assign dout3[1] = TrigBeam;
	//assign dout2_n = TrigBeam;
	//assign NIMout[0] = ~TrigBeam;
	//assign dout3_n = TrigBeam;
	//assign NIMout[2] = dout3_p;
    
//   assign NIMout[0] = FS[1];
//   assign NIMout[0] = ~TrigBeam;// PicoScope A  :  Trigger
   assign NIMout[0] = TrigBeam;
//   assign NIMout[0] = REG_DO;

//   assign NIMout[0] = DataValid;
//   assign NIMout[0] = NIMin[0];  // for SVX4 time stamp
//   assign NIMout[1] = dataoutseri;
//   assign NIMout[1] = ~LVCMOSin[0]; // PicoScope B  :  HSIO2 busy
   assign NIMout[1] = TrigBeam;
   //assign NIMout[2] = NIMin[4];  // for SVX4 trigger
//   assign NIMout[2] = TrigBit;
	assign NIMout[2] = NIMin[4];
   assign NIMout[3] = NIMin[5];
   assign NIMout[4] = NIMin[6];
     
//   assign NIMout[3] = NIMin[4]; // B REAKING  not connected 
//   assign NIMout[3] = DIN[1];
//   assign NIMout[3] = ROIBit;
   
//   assign NIMout[3] = ~(~NIMin[0] | LVCMOSin[0] | LVCMOSin[0]);// PicoScope C  :  busy
//   assign NIMout[3] = SCLK;
   
//   assign NIMout[4] = CLK100K;// to SVX4
//   assign NIMout[4] = ~NIMin[0];
   assign NIMout[5] = NIMin[7]; // to SVX4

   
//   assign NIMout[0] = ~(~NIMin[0] & ~NIMin[1] & ~NIMin[2] & ~NIMin[3]);
//   assign NIMout[1] = ~(~NIMin[1] & ~NIMin[2] & ~NIMin[3]);
//   assign NIMout[0] = ~(TrigCosmic | TrigStoppedmu | TrigDecayEle);
//   assign NIMout[1] =   ~(TrigDecayEle & ~TrigCosmic & ~TrigStoppedmu & GATEOUT);
//   assign NIMout[0] = TrigStoppedmu;
//   assign NIMout[1] = TrigDecayEle;
//   assign NIMout[0] = TrigDecayEle;
//   assign NIMout[1] = GATEOUT;


//   assign   DAC_LDAC=dac_ldac_s;
//   assign	DAC_PRE[3:0]=dac_pre_s[3:0];
//   assign	DAC_FS[3:0]=dac_fs_s[3:0];
//   assign	DAC_DIN[3:0]=dac_din_s[3:0];
//  assign	DAC_SCLK[0]=clk_20M_s;
//   assign   DAC_SCLK[1]=clk_20M_s;
//   assign	DAC_SCLK[2]=clk_20M_s;
//   assign	DAC_SCLK[3]=clk_20M_s;


   // UDP command treatment 
   parameter [7:0] STARTRUNENABLE=8'b11111010;
   parameter [7:0] STOPRUNENABLE=8'b11110101;
   parameter [7:0] CONFIGDAC=8'b11110001;
   parameter [7:0] DACLVON=8'b11110010;


//   parameter [7:0] SENDCLKTRIGGER=8'b00110011;
//   parameter [7:0] SENDMPPCTRIGGER=8'b11001100;
///////////////////////////////
// remote start
///////////////////////////////
   always@ (posedge clk_100M_s) begin
      if(SW[0]==1'b1) begin
	 RSTn_RUN<=1'b0;
      end else begin
	 debugled<=ADDR00[3:0];
         if(ADDR00==STARTRUNENABLE)begin
            RSTn_RUN<=1'b1;
         end
         if(ADDR00==STOPRUNENABLE) begin
            RSTn_RUN<=1'b0;
         end
//         if(ADDR00==CONFIGDAC) begin
//           debugled[1]<=1'b1;
//           Dac_ConfigEn<=1'b1;
//         end else begin
//            Dac_ConfigEn<=1'b0;
//         end
//         if(ADDR00==DACLVON) begin
//            Dac_WriteEn<=1'b1;
//         end else begin
//            Dac_WriteEn<=1'b0;
//         end
////         debugled[3:0]<=SIO_WD[3:0];
////         debugled[6:4]<=3'd2;
//         REG_DAC_ADD<=ADDR01[3:0];
//         REG_DAC_VAL1<=ADDR02[3:0];
//         REG_DAC_VAL2<=ADDR03[3:0];
//         REG_DAC_VAL3<=ADDR04[3:0];
      end
//      Dac_Add<=REG_DAC_ADD;
//      Dac_Data<={REG_DAC_VAL3,REG_DAC_VAL2,REG_DAC_VAL1};
//      Dac_Add<=4'b0101;
	 //      Dac_Data<=12'b101010101010;
  end

/////////////////////////////////
// switch start
/////////////////////////////////	
//   always@ (posedge clk_100M_s) begin
//      if(SW[0]==1'b1) begin
//	 RSTn_RUN<=1'b0;
//     end else begin
//	 RSTn_RUN<=1'b0;
//		end
//   end	
/////////////////////////////////

   
   // making 100K TimeStamp CLK
   parameter [9:0] CLKREDUCTION=10'd499;
   reg 			     CLK100Kr;
   reg [9:0] 		     CLK100KCNT;
   initial begin
      CLK100KCNT<=10'b0;
      CLK100Kr<=1'b1;
   end
   always@ (posedge clk_100M_s) begin
      if(CLK100KCNT!=CLKREDUCTION) 
	CLK100KCNT<=CLK100KCNT + 10'b1;
      else begin
	 CLK100KCNT<=10'b0000;
//	 if(CLK100Kr==1'b1 & RSTn_RUN)
	 if(CLK100Kr==1'b1)
	   CLK100Kr <= 1'b0;
	 else begin
	    CLK100Kr <= 1'b1;
	 end
      end
   end
	

	
	
   assign CLK100K=CLK100Kr;
   
   
   ////---------------------------------
   ////  TCP sender
   ////---------------------------------
//   (* IOB="FORCE" *) reg orUsrTxWe;
//   (* IOB="FORCE" *) reg [7:0] orUsrTxWd;
   reg orUsrRxRe;
   always@ (posedge clk_100M_s) begin
      orUsrCloseAck <= irUsrCloseReq;
      orUsrRxRe <= 1'b0;
   end

   // for SiTCP comunication
   always@ (posedge clk_100M_s) begin
      if(SW[0]==1'b1) begin
	 orUsrTxWe <= 1'b0;
	 orUsrTxWd <= 8'b0;
	 FIFODelayCNT<=4'b0;
	 DatawidthCNT<=4'b0;
      end else begin
//	 orUsrTxWe <= 1'b0;
//	 orUsrTxWd <= 8'b0;
//	 orUsrTxWe <= 1'b1;
//	 orUsrTxWd <= 8'b0101_0101;
//	 if(DataSenderReady == 1'b1 & ReadData[7:0] != 8'b0) begin
	 if(DataValid)FIFODelayCNT<=4'b1;
	 if(FIFODelayCNT!=4'b0)FIFODelayCNT<=FIFODelayCNT+4'b1;
	 if(DatawidthCNT!=4'b0)DatawidthCNT<=DatawidthCNT+4'b1;
	 if(FIFODelayCNT==4'd10) begin
	    orUsrTxWe <=1'b1;
	    FIFODelayCNT<=4'b0;
	    DatawidthCNT<=4'b1;
	 end
	 if(DatawidthCNT==4'd6)begin
	    orUsrTxWe <=1'b0;
	    DatawidthCNT<=4'b0;
	 end
	 orUsrTxWd<=ReadData;
      end // else: !if(SW[0]==1'b0)
   end // always@ (posedge clk_100M_s)

   always@ (posedge clk_100M_s) begin
      irUsrActive    <= USR_ACTIVE;
      irUsrCloseReq  <= USR_CLOSE_REQ;
      irUsrTxAfull   <= USR_TX_AFULL;
      irUsrRxEmpty   <= USR_RX_EMPTY;
      irUsrRxRv      <= USR_RX_RV;
      irUsrRxRd[7:0] <= USR_RX_RD[7:0];
   end


   // data serializer for LEMO output
   always@ (posedge clk_100M_s) begin
      if(DataValid==1'b1)begin
	 dataoutcopy<=DataOUT;
	 dataoutcnt<=7'b1;
      end else begin
	 dataoutcopy<=dataoutcopy << 1;
      end
      if(dataoutcnt!=7'b0)begin
	 dataoutcnt<=dataoutcnt+7'b1;
      	 dataoutseri<=dataoutcopy[87];
      end
      if(dataoutcnt==7'd90)dataoutcnt<=7'b0;
   end // always@ (posedge clk_100M_s)

   // command serializer for check
   always@ (posedge clk_100M_s) begin
      if(USR_RX_RV==1'b1)begin
//	 commandincopy<=USR_RX_RD;
	 commandincnt<=4'b1;
      end else begin
	 commandincopy<=commandincopy << 1;
      end
      if(commandincnt!=4'b0)begin
	 commandincnt<=commandincnt+4'b1;
      	 commandinseri<=commandincopy[7];
      end
      if(commandincnt==4'd10)commandincnt<=4'b0;
   end // always@ (posedge clk_100M_s)
   
//   IBUF #(IOSTANDARD("LVCMOS15"))
//   LVCMOSin3_MAP(.O(LVCMOSout[4]), .I(LVCMOSin[3]));


   assign USR_CLK       = clk_100M_s;
   assign USR_CLOSE_ACK = orUsrCloseAck;
   assign USR_TX_WE     = orUsrTxWe;
   assign USR_TX_WD     = orUsrTxWd[7:0];
   assign USR_RX_RE     = orUsrRxRe;
   
   
   
endmodule
