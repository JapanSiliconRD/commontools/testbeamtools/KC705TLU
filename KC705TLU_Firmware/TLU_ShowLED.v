`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:44:38 11/20/2015 
// Design Name: 
// Module Name:    TLU_ShowLED 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module TLU_ShowLED(
    input CLK0,
    input TrigCosmic,
    input TrigStoppedmu,
    input TrigDecayEle,
    output TrigCosmicLEDOUT,
    output TrigStoppedmuLEDOUT,
    output TrigDecayEleLEDOUT
    );

   parameter LEDLENGTH=24'd10000000; // x 10ns
   
   reg TrigCosmicLED;
   reg TrigStoppedmuLED;
   reg TrigDecayEleLED;
   reg [23:0] TrigCosmicLEDCNT;
   reg [23:0] TrigStoppedmuLEDCNT;
   reg [23:0] TrigDecayEleLEDCNT;
   
   initial begin
      TrigCosmicLED<=1'b0;
      TrigStoppedmuLED<=1'b0;
      TrigDecayEleLED<=1'b0;
      TrigCosmicLEDCNT<=24'b0;
      TrigStoppedmuLEDCNT<=24'b0;
      TrigDecayEleLEDCNT<=24'b0;
   end
   
   always@ (posedge CLK0)begin
      if(TrigCosmicLEDCNT != 24'b0) TrigCosmicLEDCNT<=TrigCosmicLEDCNT+24'b1;
      if(TrigCosmic) begin
	 TrigCosmicLEDCNT <= TrigCosmicLEDCNT+24'b1;
	 TrigCosmicLED<=1'b1;
      end
      if(TrigCosmicLEDCNT==LEDLENGTH) begin
	 TrigCosmicLEDCNT <= 24'b0;
	 TrigCosmicLED<=1'b0;
      end

      if(TrigStoppedmuLEDCNT != 24'b0) TrigStoppedmuLEDCNT<=TrigStoppedmuLEDCNT+24'b1;
      if(TrigStoppedmu) begin
	 TrigStoppedmuLEDCNT <= TrigStoppedmuLEDCNT+24'b1;
	 TrigStoppedmuLED<=1'b1;
      end
      if(TrigStoppedmuLEDCNT==LEDLENGTH) begin
	 TrigStoppedmuLEDCNT <= 24'b0;
	 TrigStoppedmuLED<=1'b0;
      end

      if(TrigDecayEleLEDCNT != 24'b0) TrigDecayEleLEDCNT<=TrigDecayEleLEDCNT+24'b1;
      if(TrigDecayEle) begin
	 TrigDecayEleLEDCNT <= TrigDecayEleLEDCNT+24'b1;
	 TrigDecayEleLED<=1'b1;
      end
      if(TrigDecayEleLEDCNT==LEDLENGTH) begin
	 TrigDecayEleLEDCNT <= 24'b0;
	 TrigDecayEleLED<=1'b0;
      end
   end
   assign  TrigCosmicLEDOUT = TrigCosmicLED;
   assign  TrigStoppedmuLEDOUT = TrigStoppedmuLED;
   assign  TrigDecayEleLEDOUT = TrigDecayEleLED;

endmodule
